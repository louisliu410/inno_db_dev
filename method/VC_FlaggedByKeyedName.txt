if (this.isCollection())
{
	for (int i = 0, count = this.getItemCount(); i < count; i++)
	{
		SetKeyedName(this.getItemByIndex(i));
	}
}
else
{
	SetKeyedName(this);
}
return this;
}

private void SetKeyedName(Item item)
{
	string keyedName = item.getProperty("keyed_name", "");
	System.Text.RegularExpressions.Regex rgx = new System.Text.RegularExpressions.Regex(@"^[0-9a-fA-F]{32}$", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
	if (String.IsNullOrEmpty(keyedName) || rgx.Matches(keyedName).Count > 0)
	{
		string userId = item.getProperty("flagged_by_id", "");
		if (String.IsNullOrEmpty(userId))
		{
			userId = GetFlaggedByUserId(item.getID());
		}
		Item user = GetItem("User", userId);
		keyedName = user.getProperty("keyed_name", "");
		if (String.IsNullOrEmpty(keyedName) || rgx.Matches(keyedName).Count > 0)
		{
			keyedName = user.getProperty("login_name");
		}
		item.setProperty("keyed_name", keyedName);
	}
}

private Item GetItem(string type, string id)
{
	Item itm = this.newItem(type, "get");
	itm.setID(id);
	return itm.apply();
}

private string GetFlaggedByUserId(string smFlaggedById)
{
	Item flaggedBy = GetItem("SecureMessageFlaggedBy", smFlaggedById);
	return flaggedBy.getProperty("flagged_by_id");
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='VC_FlaggedByKeyedName' and [Method].is_current='1'">
<config_id>2E258A3DEBAF48D2863529771344EBC6</config_id>
<name>VC_FlaggedByKeyedName</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
