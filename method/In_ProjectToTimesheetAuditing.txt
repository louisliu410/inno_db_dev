/*
目的:產生專案對應的專案工時審核表(In_Timesheet_Auditing)
位置: Promote或 afterUpdate
做法:
1.使用Merge 的方法產生 In_Timesheet_Auditing
*/

//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";

string strMethodName = System.Reflection.MethodBase.GetCurrentMethod().ToString();
strMethodName = strMethodName.Replace("Aras.IOM.Item methodCode(Aras.Server.Core.","");
strMethodName = strMethodName.Replace("EventArgs)","");
string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;
Item itmR = this;
	
try
{
	aml = "<AML>";
	aml += "<Item type='In_Timesheet_Auditing' action='merge' where=\"[in_project]='" + this.getID() + "'\">";
	aml += "<in_project>" + this.getID() + "</in_project>";
	aml += "<in_pm>" +  this.getProperty("owned_by_id","") + "</in_pm>";
	aml += "<owned_by_id>" +  this.getProperty("owned_by_id","") + "</owned_by_id>";
	aml += "<in_company>" + this.getProperty("in_company","") + "</in_company>";
	aml += "</Item>";
	aml += "</AML>";
	
	itmR = inn.applyAML(aml);
}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

	string strError = ex.Message + "\n";
	strError += (ex.InnerException==null?"":ex.InnerException.Message + "\n");

	if(aml!="")
	{
	strError += "無法執行AML:" + aml  + "\n";;
	}
	if(sql!="")
	{
		strError += "無法執行SQL:" + sql  + "\n";;
	}	
	string strErrorDetail="";
	strErrorDetail = strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
	//return inn.newError(_InnH.Translate(strError));
	throw new Exception(strError);
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_ProjectToTimesheetAuditing' and [Method].is_current='1'">
<config_id>C0E0E7320C98424B8F34C8C13C51B0E9</config_id>
<name>In_ProjectToTimesheetAuditing</name>
<comments>產生專案對應的專案工時審核表(In_Timesheet_Auditing)</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
