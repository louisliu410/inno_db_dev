var templateNames = parent.dialogArguments.names;
  var projectTemplate = document.getElementById('projectTemplate');
if (!templateNames || templateNames.length === 0) 
{
  document.getElementById('useTemplate_no').checked = true;
  document.getElementById('useTemplate_yes').checked = false;
  document.getElementById('useTemplate_yes').disabled = true;
  projectTemplate.disabled = true;
}
else
{
  document.getElementById('useTemplate_no').checked = false;
  document.getElementById('useTemplate_yes').checked = true;
  document.getElementById('useTemplate_yes').disabled = false;
  document.getElementById('projectTemplate').disabled = false;
  projectTemplate.options[0].selected = true;
}

projectTemplate.parentNode.className = projectTemplate.disabled ? 'sys_f_div_select sys_f_div_select_disabled': 'sys_f_div_select';
top.aras.updateDomSelectLabel(projectTemplate);

var input = getObject("name", "input");
if (input)
{

  var cd = "var spn = getObject('name_duplicated_value_inner_span', 'span');\n";
  cd += "if (spn)\n";
  cd += "{\n";
  cd += "  var val = getObject('name', 'input');\n";
  cd += "  if (val)\n";
  cd += "  {\n";
  cd += "    val = val.value;\n";
  cd += "    if (!val) val = '';\n";
  cd += "    parent.setToolbarControlEnabled('sendData', (val ? true : false));\n";
  cd += "    spn.innerHTML = val;\n";
  cd += "  }\n";
  cd += "}\n";
  cd += "return true;\n";
  cd += getObject.toString();
  var fnc = new Function(cd);
  input.addEventListener("keyup", fnc, false);
  input.focus();
}

function getObject(name, tagName)
{
  var tags = document.getElementsByTagName(tagName);
  for (var i = 0; i < tags.length; i++)
  {
    if (tags[i].getAttribute("name") == name) {return tags[i];}
  }
}
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='PM_projectOnAdd_onLoad' and [Method].is_current='1'">
<config_id>A4BD67AC627A41D58D040D7E14CF741A</config_id>
<name>PM_projectOnAdd_onLoad</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
