/*
    取得當下登入的使用者所屬的身分群組。
    回傳一個collection類型的物件。
*/

    Innovator inn=this.getInnovator();
	Item identity=inn.newItem("Identity","get");
	identity.setAttribute("select","name");
	identity.setProperty("id",Aras.Server.Security.Permissions.Current.IdentitiesList);
	identity.setPropertyCondition("id","in");
	identity=identity.apply();
	
	

return identity;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Get_DisplayIdentityList' and [Method].is_current='1'">
<config_id>332127EEC9E644DF924B33FB7B89A437</config_id>
<name>In_Get_DisplayIdentityList</name>
<comments>取得使用者所屬的腳色群組清單，主要由leftmenu.aspx使用。</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
