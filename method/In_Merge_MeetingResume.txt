/*
1.先取得In_Meeting的id
2.取得In_Meeting_User備註說明為正取的Item(MeetingUsers)
3.用for去遍歷每個MeetingUsers中的MeetingUser
   3.1取得該MeetingUser的ID(MeetingUserId)
   3.2取得In_Meeting_Resume的in_user與MeetingUserId相符的Item(MeetingResume)
	   若無則用Add在In_Meeting_Resume創建一筆Item並將其in_user設為MeetingUserId
*/
//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();
int lockStatus = this.fetchLockStatus();
if(lockStatus != 0)
{
    throw new Exception("請解鎖!");
}
string MeetingId = this.getID();
Item MeetingUsers = inn.newItem("In_Meeting_User", "get");
MeetingUsers.setProperty("source_id", MeetingId);
MeetingUsers.setProperty("in_note_state", "official");
MeetingUsers = MeetingUsers.apply();//獲得in_Meeting_User中正取的Item
string Condition1 = "沒有資料更新!\n", Condition2 = "本次資料建立完成!\n";
string result = "";
int count = 0;
Item itmResult;



/***********************/
Item ResumeList = inn.newItem("In_Meeting_Resume", "get");
ResumeList.setAttribute("where", "[In_Meeting_Resume].source_id='" + MeetingId + "'");
ResumeList = ResumeList.apply();
int num = ResumeList.getItemCount();
string[] list = new string[num];
for (int i = 0; i < num; i++)
{
	Item Resume = ResumeList.getItemByIndex(i);
	list[i] = Resume.getProperty("id");
}

Item ResumeDels = inn.newItem("In_Meeting_Resume", "delete");
for (int i = 0; i < num; i++)
{
	ResumeDels.setID(list[i]);
	ResumeDels.apply();
}

Item check = inn.newItem("In_Meeting_Resume", "get");
check.setAttribute("where", "[In_Meeting_Resume].source_id='" + MeetingId + "'");
check = check.apply();


/***********************/

for (var i = 0; i < MeetingUsers.getItemCount(); i++)
{
	Item MeetingUser = MeetingUsers.getItemByIndex(i);//MeetingUsers的每個Item,即MeetingUser
	string MeetingUserId = MeetingUser.getID();//獲得此筆MeetingUser的ID
	Item MeetingResume = inn.newItem("In_Meeting_Resume", "get");
	MeetingResume.setProperty("source_id", MeetingId);
	MeetingResume.setAttribute("where", "[In_Meeting_Resume].in_user='" + MeetingUserId + "' AND [In_Meeting_Resume].source_id='" + MeetingId + "'");
	MeetingResume = MeetingResume.apply();//獲得MeetingResume中參與者和此筆MeetingUser的ID相符的Item


	if (MeetingResume.isError())//查詢不到則在MeetingResume中以MeetingUSer的ID和指定In_Meeting的ID來創建此筆資料
	{
		count++;

		MeetingResume = inn.newItem("In_Meeting_Resume", "add");
		MeetingResume.setProperty("in_user", MeetingUserId);
		MeetingResume.setProperty("source_id", MeetingId);
		MeetingResume = MeetingResume.apply();
		result = result + MeetingResume.getProperty("in_name") + ", ";
	}

}



if (count == 0)
{
	itmResult = inn.newResult(Condition1);
}
else
{
	itmResult = inn.newResult(Condition2 + result + "共計" + count + "筆資料");
}
return itmResult;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Merge_MeetingResume' and [Method].is_current='1'">
<config_id>25FD509B06994F78BCCDCC43ACCB489B</config_id>
<name>In_Merge_MeetingResume</name>
<comments>In_Meeting_User的備註說明為正取的資料merge到In_Meeting_Resume</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
