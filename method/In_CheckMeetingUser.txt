
Innovator inn=this.getInnovator();

string email=this.getProperty("email","tanchi@innosoft.com.tw");
string meeting_id=this.getProperty("meeting_id","6908863025B74A008F8A8C1A88E2FBBF");

string result="";
Item meetingUserQ=inn.newItem();

Item meeting=inn.newItem();
meeting.setID(meeting_id);
meeting.setAttribute("type","In_Meeting");
meeting.setAttribute("action","get");
meeting=meeting.apply();
meeting.fetchRelationships("In_Meeting_User");

Item meetingUsers=meeting.getRelationships("In_Meeting_User");
int count=meetingUsers.getItemCount();
for(int i=0;i<count;i++){
    Item meetingUser=meetingUsers.getItemByIndex(i);
    if(meetingUser.getProperty("in_mail","")==email){
        result=meetingUser.getID();
    }
    
}

return inn.newResult(result);
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_CheckMeetingUser' and [Method].is_current='1'">
<config_id>49947503AB704B9F8C9742E22ED51CA8</config_id>
<name>In_CheckMeetingUser</name>
<comments>檢查傳入的email是否已報名過</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
