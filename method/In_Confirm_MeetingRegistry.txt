//更新與會者的參加狀況(允許參加)

Innovator inn=this.getInnovator();

//處理傳入的各項資料
string param=this.getProperty("parameter","no_data");//取得傳入的parameter
string meeting_id=this.getProperty("meeting_id","no_data");
//檢查傳入參數
//如果parameter取到no_data，傳回錯誤並結束處理
if(param=="no_data" || param=="" ||meeting_id=="no_data" ){ 
    return inn.newError("Invalid parameter param=  "+param +"  & meeting_id=   "+meeting_id);   }
//System.Diagnostics.Debugger.Break();
string[] pms=param.Split('&');//處理傳入的parameter，但保留key=value的架構。

List<Item> movingUsers=new List<Item>();
for(int j=0;j<pms.Length;j++){
    string[] kvp=pms[j].Split('=');
    Item applyMethod=inn.newItem();
    //從傳入的資料建立物件
    Item meetingUser=inn.newItem();
    meetingUser.setAttribute("type","In_Meeting_User");
    meetingUser.setAttribute("action","edit");
    meetingUser.setID(kvp[0]);
    //假設傳入的meetingID與該In_Meeting_User物件所屬的會議不同，則是為要轉移到別的會議，在這邊先設為不允許參加。
    if(kvp[1]==meeting_id){
        meetingUser.setProperty("in_allowed","1");
        meetingUser=meetingUser.apply();
        
        applyMethod.setAttribute("type","Method");
        applyMethod.setAttribute("action","In_Send_MeetingConfirmation");
        applyMethod.setPropertyItem("meeting_user",meetingUser);
        applyMethod.apply();
        continue;
    }
    if(kvp[1]=="not_allowed"){
        meetingUser.setProperty("in_allowed","0");
        meetingUser=meetingUser.apply();
        continue;
    }
    if(kvp[1]!=meeting_id && kvp[1]!="not_allowed"){
        meetingUser.setProperty("in_allowed","0");
        meetingUser=meetingUser.apply();
        applyMethod.setAttribute("type","Method");
        applyMethod.setAttribute("action","In_Move_MeetingUser");
        applyMethod.setPropertyItem("meeting_user",meetingUser);
        applyMethod.setProperty("tgt_meeting",kvp[1]);
        Item movedUser=applyMethod.apply();
        
        //寄送確認函
        applyMethod.setPropertyItem("meeting_user",movedUser);
        applyMethod.setAttribute("action","In_Send_MeetingConfirmation");
        applyMethod.apply();
    }
    
}



return inn.newResult("ok");//當一切正常時，回傳ok作為任務成功的訊號
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Confirm_MeetingRegistry' and [Method].is_current='1'">
<config_id>59FD2880113F4952B6C83B866D128B72</config_id>
<name>In_Confirm_MeetingRegistry</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
