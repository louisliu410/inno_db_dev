var query = this.newItem('Variable', 'get');
// Force.Disable.SSVC.Notifications
query.setID('98A71F83992449D1ACE157F0E0929CCD');
query = query.apply();
if (query.isError()) {
	aras.AlertError(query);
	return;
}

var isNotificationsDisabled = query.getProperty('value') === '1';
var resourceKey;
if (isNotificationsDisabled) {
	resourceKey = 'enable_notifications';
} else {
	resourceKey = 'disable_notifications';
}
var warningText = aras.getResource('../Modules/aras.innovator.SSVC/', resourceKey);
if (!aras.confirm(warningText)) {
	return;
}

query.setProperty('value', isNotificationsDisabled ? '0' : '1');
query = query.apply('edit');
if (query.isError()) {
	aras.AlertError(query);
	return;
}

query = this.newItem('Method', 'VCN_CreateEmailDigestTask');
query.setAttribute('check_existence', '1');
query = query.apply();
if (query.isError()) {
	aras.AlertError(query);
	return;
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='VCN_SwitchNotificationsState' and [Method].is_current='1'">
<config_id>E5495E51F2ED4C448052792970D3A02B</config_id>
<name>VCN_SwitchNotificationsState</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
