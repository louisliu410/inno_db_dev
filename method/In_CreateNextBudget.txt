string strMethodName = "In_CreateNextBudget";
/*
目的:建立次年的預算表
做法:
先判斷次年度的預算表是否已存在
1.只要專案上面立案單的狀態沒推到”財務結案”，都要產生一個次年專案預算表
2.所有數字欄位預設0
3.把工時預算13~24月變成下一預算的1~12，下一年的13~24歸零；複製四欄位(預計總工時、預計總費用、累計已花工時、累計已花費用)，歸零2欄位(本年累計工時、本年累計費用)。
*/

//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";


Item itmR = this;

try
{
	string IgnoreProperties="";
	Item itmBudget = this;
	int intNextYear = Convert.ToInt32(this.getProperty("in_year","0"));
	string strNextYear = (intNextYear+1).ToString();
	if(strNextYear=="")
	    throw new Exception("年份不得為空白");

	//strNextYear = "2017";

	//先判斷次年度的預算表是否已存在
	aml = "<AML>";
	aml += "<Item type='In_Budget' action='get'>";
	aml += "<in_year>" + strNextYear + "</in_year>";
	aml += "<in_refproject>" + this.getProperty("in_refproject","") + "</in_refproject>";
	aml += "</Item></AML>";
	Item itmNewBudget = inn.applyAML(aml);
	if(!itmNewBudget.isError())
		return inn.newResult("次年度預算表已存在,因此取消創建");

	itmNewBudget = inn.newItem("In_Budget","add");
	IgnoreProperties = "in_refbudget,in_year";
	string strCopyProperties = _InnH.GetAllPropertyNames("In_Budget", "CanCopy", IgnoreProperties);
	string[] arrCopyProperties = strCopyProperties.Split(',');
	strCopyProperties = "";
	for (int i=0;i< arrCopyProperties.Length;i++)
	{
		strCopyProperties += arrCopyProperties[i] + "," + arrCopyProperties[i] + ";";
	}
	strCopyProperties = strCopyProperties.Trim(';');
	itmNewBudget.setProperty("in_refbudget",itmBudget.getID());
	itmNewBudget.setProperty("in_year",strNextYear);
	itmNewBudget = _InnH.CopyProperties(itmBudget, itmNewBudget, strCopyProperties, "0", false);

	string strCopyRelationships = "In_Budget_Staff_Budget,In_Budget_RD_Budget,In_Budget_parameter";
	string[] arrCopyRelationships = strCopyRelationships.Split(',');
	for (int i=0;i<arrCopyRelationships.Length;i++)
	{
		strCopyProperties = "";
		if(arrCopyRelationships[i]=="In_Budget_Staff_Budget")
		{
		    for(int k=13;k<=24;k++)
			{
				strCopyProperties += "in_timerecord_budget_" +k.ToString() + ",";
			}

			for(int l=1; l<=12; l++)
			{
				strCopyProperties += "in_budget_" +l.ToString() + ",";
				strCopyProperties += "in_real_" +l.ToString() + ",";
			}

			strCopyProperties += "related_id,in_rank,in_userstr,in_note,in_timerecord_budget,in_timerecord_real_total";
		}
		if(arrCopyRelationships[i]=="In_Budget_RD_Budget")
		{
			strCopyProperties = "in_budget_1,in_note,related_id";
		}
		if(arrCopyRelationships[i]=="In_Budget_parameter")
		{
			strCopyProperties = "in_multi,in_company,in_parameter";
		}


		arrCopyProperties = strCopyProperties.Split(',');
		strCopyProperties = "";
		for (int k = 0;k < arrCopyProperties.Length; k++)
		{
			strCopyProperties += arrCopyProperties[k] + "," + arrCopyProperties[k] + ";";
		}
		strCopyProperties = strCopyProperties.Trim(';');
		_InnH.CopyRelationships(arrCopyRelationships[i], itmBudget.getID(), arrCopyRelationships[i], itmNewBudget.getID(), strCopyProperties);
	}

	//把工時預算13~24月變成下一預算的1~12，下一年的13~24歸零；複製四欄位(預計總工時、預計總費用、累計已花工時、累計已花費用)，歸零2欄位(本年累計工時、本年累計費用)。

	sql = "Update [In_Budget_Staff_Budget] set ";
	sql += "in_timerecord_budget_1=in_timerecord_budget_13";
	sql += ",in_timerecord_budget_2=in_timerecord_budget_14";
	sql += ",in_timerecord_budget_3=in_timerecord_budget_15";
	sql += ",in_timerecord_budget_4=in_timerecord_budget_16";
	sql += ",in_timerecord_budget_5=in_timerecord_budget_17";
	sql += ",in_timerecord_budget_6=in_timerecord_budget_18";
	sql += ",in_timerecord_budget_7=in_timerecord_budget_19";
	sql += ",in_timerecord_budget_8=in_timerecord_budget_20";
	sql += ",in_timerecord_budget_9=in_timerecord_budget_21";
	sql += ",in_timerecord_budget_10=in_timerecord_budget_22";
	sql += ",in_timerecord_budget_11=in_timerecord_budget_23";
	sql += ",in_timerecord_budget_12=in_timerecord_budget_24";
	sql += " where source_id='" + itmNewBudget.getID() + "'";
	inn.applySQL(sql);

	sql = "Update [In_Budget_Staff_Budget] set ";
	sql += "in_timerecord_budget_13=0";
	sql += ",in_timerecord_budget_14=0";
	sql += ",in_timerecord_budget_15=0";
	sql += ",in_timerecord_budget_16=0";
	sql += ",in_timerecord_budget_17=0";
	sql += ",in_timerecord_budget_18=0";
	sql += ",in_timerecord_budget_19=0";
	sql += ",in_timerecord_budget_20=0";
	sql += ",in_timerecord_budget_21=0";
	sql += ",in_timerecord_budget_22=0";
	sql += ",in_timerecord_budget_23=0";
	sql += ",in_timerecord_budget_24=0";
	sql += " where source_id='" + itmNewBudget.getID() + "'";
	inn.applySQL(sql);

	itmR = itmNewBudget.apply("edit");

	sql = "Update [In_Budget_Staff_Budget] set ";
	sql += "in_real_1=0";
	sql += ",in_real_2=0";
	sql += ",in_real_3=0";
	sql += ",in_real_4=0";
	sql += ",in_real_5=0";
	sql += ",in_real_6=0";
	sql += ",in_real_7=0";
	sql += ",in_real_8=0";
	sql += ",in_real_9=0";
	sql += ",in_real_10=0";
	sql += ",in_real_11=0";
	sql += ",in_real_12=0";
	sql += ",in_budget_1=0";
	sql += ",in_budget_2=0";
	sql += ",in_budget_3=0";
	sql += ",in_budget_4=0";
	sql += ",in_budget_5=0";
	sql += ",in_budget_6=0";
	sql += ",in_budget_7=0";
	sql += ",in_budget_8=0";
	sql += ",in_budget_9=0";
	sql += ",in_budget_10=0";
	sql += ",in_budget_11=0";
	sql += ",in_budget_12=0";
	sql += ",in_real=0";
	sql += ",in_budget=0";
	sql += ",in_budget_after_after=0";
	sql += ",in_real_total_pre=in_real_total";
	sql += ",in_budget_total_pre=in_budget_total";
	sql += " where source_id='" + itmNewBudget.getID() + "'";
	inn.applySQL(sql);

	sql = "Update [In_Budget] set ";
	sql += "in_total_rate=0,in_total_budget=0,in_total_currentcost=0,";
	sql += "in_staff_rate=0,in_staff_budget=0,in_staff_currentcost=0,";
	sql += "in_outsource_rate=0,in_outsource_budget=0,in_outsource_currentcost=0,";
	sql += "in_other_rate=0,in_other_budget=0,in_other_currentcost=0,";
	sql += "in_total_rate_f=0,in_total_budget_f=0,in_total_currentcost_f=0,";
	sql += "in_staff_rate_f=0,in_staff_budget_f=0,in_staff_currentcost_f=0,";
	sql += "in_outsource_rate_f=0,in_outsource_budget_f=0,in_outsource_currentcost_f=0,";
	sql += "in_other_rate_f=0,in_other_budget_f=0,in_other_currentcost_f=0";
	sql += " where id='" + itmNewBudget.getID() + "'";
	inn.applySQL(sql);



}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);


	string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;


	string strError = ex.Message + "\n";
	//strError += (ex.InnerException==null?"":ex.InnerException.Message + "\n");

	if(aml!="")
    	strError += "無法執行AML:" + aml  + "\n";

	if(sql!="")
		strError += "無法執行SQL:" + sql  + "\n";

	string strErrorDetail="";
	strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
	strError = strError.Replace("\n","</br>");
	throw new Exception(_InnH.Translate(strError));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_CreateNextBudget' and [Method].is_current='1'">
<config_id>B48814733B99466984504C474AC596C9</config_id>
<name>In_CreateNextBudget</name>
<comments>建立單筆次年的預算表</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
