//System.Diagnostics.Debugger.Break();
/*
目的:在 Downloader 被推到 "Offline conversion" 的時後,創建一個 In_Queue
*/

Innovator inn = this.getInnovator();
string aml = "";

//創建 In_Queue 的 AML
string strCreateQueue = "";
strCreateQueue += "<AML>";
strCreateQueue += "<Item type='In_Queue' action='add'>";
strCreateQueue += "<in_source_itemtype>" + this.getType() + "</in_source_itemtype>";
strCreateQueue += "<in_source_id>" + this.getID() + "</in_source_id>";
strCreateQueue += "<in_source_keyed_name>@keyed_name</in_source_keyed_name>";
strCreateQueue += "<in_type>batchdownload</in_type>";
strCreateQueue += "<in_state>start</in_state>";
strCreateQueue += "<in_inparam><![CDATA[@in_inparam]]></in_inparam>";
strCreateQueue += "<name>" + this.getProperty("name","") + "</name>";
strCreateQueue += "<owned_by_id>" + this.getProperty("owned_by_id","") + "</owned_by_id>";

//strCreateQueue += "<in_state_method>";
//strCreateQueue += "<Item type='Method' action='get' select='id'>";
//strCreateQueue += "<name>In_Queue_batchdownload</name>";
//strCreateQueue += "</Item>";
//strCreateQueue += "</in_state_method>";
strCreateQueue += "</Item></AML>";

string in_inparam="<in_param><downloader_id>" + this.getID() + "</downloader_id></in_param>";

string strKeyed_name = "";
strKeyed_name = this.getProperty("keyed_name");

strCreateQueue = strCreateQueue.Replace("@in_inparam",in_inparam);
strCreateQueue = strCreateQueue.Replace("@keyed_name",strKeyed_name);


Item In_Queue = inn.applyAML(strCreateQueue);
if(In_Queue.isError())
	throw new Exception(In_Queue.getErrorString());
return this;



#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_CreateDownloadQueue' and [Method].is_current='1'">
<config_id>BE296A1FA511449C8D4735F3E083D3F6</config_id>
<name>In_CreateDownloadQueue</name>
<comments>inn core</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
