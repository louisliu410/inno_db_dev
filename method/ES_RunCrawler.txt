if (this.getProperty('crawler_state', '') === 'Active') {

	var crawlerState = this.newItem('ES_CrawlerState', 'get');
	crawlerState.setAttribute('select', 'current_action, next_action');
	crawlerState.setProperty('source_id', this.getID());
	crawlerState = crawlerState.apply();

	if (!crawlerState.isError() && crawlerState.getItemCount() === 1) {

		if (crawlerState.getProperty('current_action', '') === 'Stop current iteration') {
			crawlerState.setProperty('next_action', 'Run');

			var iLockStatus = crawlerState.getLockStatus();

			if (iLockStatus === 0) {
				crawlerState.apply('edit');
			}

			if (iLockStatus === 1) {
				crawlerState.apply('update');
			}
		} else {
			aras.AlertError('The crawler is already running.');
		}
	} else {
		aras.AlertError(crawlerState.getErrorMessage());
	}
} else {
	aras.AlertError('A crawler should be in \'Active\' state to perform any operations.');
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='ES_RunCrawler' and [Method].is_current='1'">
<config_id>BCE1DBEAB36F41B6A36DDF38CA0852E4</config_id>
<name>ES_RunCrawler</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
