var topWindow = aras.getMostTopWindowWithAras(window);
var workerFrame = topWindow.work;
if (workerFrame && workerFrame.searchContainer) {
	workerFrame.searchContainer._saveSearch();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwmm_saveSavedSearch' and [Method].is_current='1'">
<config_id>B44D1E9F65544B4594F5FD8C2996391B</config_id>
<name>cui_default_mwmm_saveSavedSearch</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
