var topWindow = aras.getMostTopWindowWithAras(window);
var menuFrame = topWindow.menu;
if (menuFrame && menuFrame.onDefaultDStateCommand) {
	localStorage.setItem('defaultDState', 'defaultDState');
	menuFrame.onDefaultDStateCommand();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwmm_onDefaultDState' and [Method].is_current='1'">
<config_id>E1A101C77D8D43A283B4325EE0C2E796</config_id>
<name>cui_default_mwmm_onDefaultDState</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
