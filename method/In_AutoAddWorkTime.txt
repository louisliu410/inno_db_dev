/*
目的:前一個工作日未填寫工時卡便自動新增並發送通知
做法:

位置:
InnoSchedule
*/

//System.Diagnostics.Debugger.Break();

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Timesheet Manager");
bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Item itmR = this;

string aml = "";
string sql = "";

string strQueueID = this.getProperty("queue_id","");

if(strQueueID!=""){
    
}




/*
//建立空白的工時紀錄表(前一工作日)
aml = "<AML>";
aml += "<Item type='Identity' action='get'>";
aml += "<id condition='in'>"+strUserID+"</id>";
aml += "</Item></AML>";
itmIdentities = inn.applyAML(aml);
for(int i=0;i<itmIdentities.getItemCount();i++){
    Item itmIdentity = itmIdentities.getItemByIndex(i);
    
    aml = "<AML>";
    aml += "<Item type='In_TimeRecord' action='add'>";
    aml += "<in_company>"+itmIdentity.getProperty("in_company","")+"</in_company>";
    aml += "<in_project>";
    aml += "<Item type='Project' action='get'>";
    aml += "<in_number>99999</in_number>";	
    aml += "</Item>";
    aml += "</in_project>";
    aml += "<in_start_time>"+strWorkDays+"</in_start_time>";
    aml += "<in_today_hours>0</in_today_hours>";	
    aml += "<owned_by_id>"+itmIdentity.getID()+"</owned_by_id>";
    aml += "<in_function_code>";
    aml += "<Item type='In_Function_Code' action='get'>";
    aml += "<in_code>L00</in_code>";
    aml += "</Item>";
    aml += "</in_function_code>";
    aml += "<description>請盡快填寫工時紀錄表</description>";
    aml += "<in_work_hours>0</in_work_hours>";
    aml += "</Item></AML>";
    Item itmTimeRecord = inn.applyAML(aml);
    itmTimeRecord.promote("Approved","");
    
    //6.取得員工(本身,主管,人事)
	strStaff = itmIdentity.getID();
	
	string strToAdmin = "0B08F23C06D543388C844467F53E3974";//Timesheet Manager

 	if(itmIdentity.getProperty("owned_by_id","") != ""){
 		if(itmIdentity.getProperty("owned_by_id","") != "F6FCB8E66EBE4FCF8E0E73BD78E024CE")//董事長
 			strToAdmin = strToAdmin + "," + itmIdentity.getProperty("owned_by_id","");
 	}
 	
 	//strMsg = "標題：PLM系統自動提醒通知-未填寫 " + strWorkDays + " 工時紀錄表\n內容：與標題相同";
 	string strSubject="PLM系統自動提醒通知-" + itmIdentity.getProperty("keyed_name","") + "未填寫 " + strWorkDays + " 工時紀錄表";
	
	//未填寫 2017-01-10 工時紀錄表，若已填寫請忽略此信件。
	string strMsgUser = "未填寫 " + strWorkDays + "  工時紀錄表，若已填寫請忽略此信件。";
	
	//員工：郭甄蓁1690 未填寫 2017-01-10 工時紀錄表。
	string strMsgAdmin = "員工：" + itmIdentity.getProperty("keyed_name","") + "未填寫 " + strWorkDays + " 工時紀錄表";
	
	//7.寄出通知信
	_InnH.SendEmail(strSubject,strMsgUser,strStaff);
	_InnH.SendEmail(strSubject,strMsgAdmin,strToAdmin);
	strResult += itmIdentity.getProperty("keyed_name","") + ",";
}*/
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

//return inn.newResult("產生以下人員" + strWorkDays + "的空白工時卡:" + strResult);
return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_AutoAddWorkTime' and [Method].is_current='1'">
<config_id>36D634328FC243B2B34FBA6504F2FD17</config_id>
<name>In_AutoAddWorkTime</name>
<comments>送你一張工時卡</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
