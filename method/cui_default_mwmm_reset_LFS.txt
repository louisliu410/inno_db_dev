var topWindow = aras.getMostTopWindowWithAras(window);
var workerFrame = topWindow.work;
var itemID = workerFrame.grid.getSelectedId();
var res = aras.resetLifeCycle(workerFrame.itemTypeName, itemID);
if (!res) {
	aras.AlertError(aras.getResource('', 'mainmenu.reset_lcs_failed'));
	return false;
} else {
	var win = aras.uiFindWindowEx(itemID);
	if (win) {
		win.document.location.reload();
	}

	if (workerFrame.isItemsGrid) {
		workerFrame.updateRow(aras.getItemById('', itemID, 0));
	}
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwmm_reset_LFS' and [Method].is_current='1'">
<config_id>3F718EBE2D2945688893106C734E8C77</config_id>
<name>cui_default_mwmm_reset_LFS</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
