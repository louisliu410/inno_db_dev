// //System.Diagnostics.Debugger.Break();
/*
目的:流程節點推動之後,執行必要的工作,例如重新繪製流程圖
位置:Workflow Process的 lifecycle transition 上的  post-method
作法:
1.執行 In_UpdateFlowchart
*/

Innovator inn = this.getInnovator();
string strType = this.getType();
string strState = this.getProperty("state");
string sql = "";

string aml = "";





string strCreatorIdenId = "";

Item itmWorkflowForm = null;

//這裡換身分一定要用 Aras PLM,因為推動 lifecycle 都是用 Aras PLM 執行的
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Aras PLM");
Aras.Server.Security.Identity plmIdentity1 = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);
    PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity1);



Item ControlledItem = null;
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

string strFromState = this.getPropertyItem("transition").getPropertyAttribute("from_state","keyed_name");
string strToState = this.getPropertyItem("transition").getPropertyAttribute("to_state","keyed_name");
string strFromTo = strFromState + "_to_" + strToState;

bool IsInnoAssignment=false;

Item itmActAss ;	
if(strType=="Activity")
{
	switch(strToState)
	{
		case "Active":						

			Item itmWFP = _InnH.GetInnoWorkflowProcessByActivity(this);
            itmWFP.apply("In_UpdateFlowchart");

			break;
		case "Cancelled":
		case "Closed":
		break;

	}

}
else
{
	//更新workflow process內容
	Item itmWFP = _InnH.GetInnoWorkflowProcessByWorkflowProcess(this);
	itmWFP.apply("In_UpdateFlowchart");
}

if (PermissionWasSet)
{
    Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
    Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity1);
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Activity_Trans_Post' and [Method].is_current='1'">
<config_id>00A1087B6C1A46FF804CECE2703A2C48</config_id>
<name>In_Activity_Trans_Post</name>
<comments>inn core</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
