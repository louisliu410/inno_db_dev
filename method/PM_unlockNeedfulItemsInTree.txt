Dim tmpInn As Innovator = Me.getInnovator()
Dim wbs_id As String = Me.getProperty("wbs_id")

If wbs_id Is Nothing Then
  Dim conn As Aras.Server.Core.InnovatorDatabase = CCO.Variables.InnDatabase
  Dim tableName As String = conn.GetTableName(Me.getType().Replace(" ", "_"))
  Dim rs As Aras.Server.Core.InnovatorDataSet = conn.ExecuteSelect( _
    "SELECT wbs_id " + _
    "FROM " + tableName + " " + _
    "WHERE id='" + Me.getAttribute("id").Replace("'", "''") + "'")
    If Not rs.EoF Then
      wbs_id = CStr(rs.Value(0, Nothing))
    End If
End If

If Not wbs_id Is Nothing Then
  Return tmpInn.applyMethod("Unlock Project Tree", String.Format("<body top_wbs_id='{0}' handle_locked_by_current_user='1'/>", wbs_id))
End If

Return Me
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='PM_unlockNeedfulItemsInTree' and [Method].is_current='1'">
<config_id>9DF72F72548F4124905B86A8A39594AB</config_id>
<name>PM_unlockNeedfulItemsInTree</name>
<comments>Unlocks all items in the Project Tree those are locked by the current user</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
