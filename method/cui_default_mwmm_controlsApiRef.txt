var topWindow = aras.getMostTopWindowWithAras(window);
var menuFrame = topWindow.menu;
if (menuFrame && menuFrame.onShowControlsApiReferenceDotNetCommand) {
	menuFrame.onShowControlsApiReferenceDotNetCommand();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwmm_controlsApiRef' and [Method].is_current='1'">
<config_id>A545B20F4789445C81010509FA13326B</config_id>
<name>cui_default_mwmm_controlsApiRef</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
