Innovator inn = this.getInnovator();
string aml = "<AML>";
aml += "<Item type='Identity' action='get'>";
aml += "<is_alias>1</is_alias>";
aml += "</Item>";
aml += "</AML>";
Item itmAllAliass = inn.applyAML(aml);
string strR = "";
for(int i=0;i<itmAllAliass.getItemCount();i++)
{
	Item itmAllAlias = itmAllAliass.getItemByIndex(i);
	strR += itmAllAlias.getProperty("name") + "-------------------------\n";
	strR += inn.applyMethod("In_ShowMyOrg_S","<id>" + itmAllAlias.getID() + "</id>").getResult() + "\n";
	
	
}
return inn.newResult(strR);

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_ShowAllUserOrg_S' and [Method].is_current='1'">
<config_id>911306FA4FA048C6A584165E65F90B11</config_id>
<name>In_ShowAllUserOrg_S</name>
<comments>inn tool 顯示全公司每個人的所屬組織與一二階主管</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
