string yearmonth  = ""; //可在此強制指定重整的月份
// string yearmonth  = "2017-10";

string strMethodName = "In_Calculate_TimeRecordCost";
/*
目的:計算本月的工時卡的人力成本
傳入參數:<yearmonth>2016-09</yearmonth>
做法:
A.更新個別工時卡的成本
1.取得各User的時薪,填寫到in_user_cost
2.找到區間內的所有工時卡,計算每張工時卡的人力成本(工時*時薪),不四捨五入

B.Merge人事預算頁簽中各月的實際工時與費用
1.將實際人事費塞到各預算表內(須確保系統內有相同公司別的 salary 會計科目,以及 salary會計科目對應的 part, PART編號與名稱不拘)
2.將實際工時塞到各預算表內

C.更新各專案的表頭總工時欄位

觸發時機點:In_MergeUserCost 收到薪資之後執行
*/

//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";
Item itmR = this;
string strErrorMessage = "";
string str_r = "";


try
{
	//若沒指定 yearmonth 假屬性,則以執行的當月份為計算基礎
	if(yearmonth=="")
		yearmonth = this.getProperty("yearmonth",System.DateTime.Now.AddMonths(-1).ToString("yyyy-MM"));


	Item itmBudgetTimePeriod = inn.applyMethod("In_Budget_GetTimePeriod","<yearmonth>" + yearmonth + "</yearmonth>");
	DateTime dtStaffEndDate = Convert.ToDateTime(itmBudgetTimePeriod.getProperty("staff_enddate"));    
	DateTime dtStaffStartDate = Convert.ToDateTime(itmBudgetTimePeriod.getProperty("staff_startdate"));

	string strMonth1 = dtStaffEndDate.ToString("%M");
	string strMonth2 = dtStaffEndDate.ToString("MM");



	//2.取得各User的時薪,填寫到in_user_cost
	sql = "select distinct owned_by_id from In_TimeRecord where ";
	sql += " (in_start_time between '" + dtStaffStartDate.AddHours(-8).ToString("yyyy-MM-dd HH:mm:ss")  + "'";
	sql += " and '" + dtStaffEndDate.AddHours(-8).ToString("yyyy-MM-dd HH:mm:ss") + "')";
	Item itmIdentityIds = inn.applySQL(sql);
	string strIdentityIds = _InnH.GetCSV(itmIdentityIds,"owned_by_id");
	strIdentityIds = "'" + strIdentityIds.Replace(",","','") + "'";

	aml = "<AML>";
	aml += "<Item type='Alias' action='get' select='source_id(in_cost),related_id'>";	
	aml += "<related_id condition='in'>" + strIdentityIds + "</related_id>";
	aml += "</Item></AML>";
	Item itmAliass = inn.applyAML(aml);
	for(int i=0;i<itmAliass.getItemCount();i++)
	{
		Item itmAlias = itmAliass.getItemByIndex(i);		

		string strCost = itmAlias.getPropertyItem("source_id").getProperty("in_cost","0");
		if(strCost=="")
			throw new Exception("使用者:" + itmAlias.getPropertyAttribute("source_id","keyed_name","") + "無時薪資料,請更新之");

		sql = "Update In_TimeRecord set in_user_cost = " + strCost + " where owned_by_id='" + itmAlias.getProperty("related_id") + "'";
		sql += " and (in_start_time between '" + dtStaffStartDate.AddHours(-8).ToString("yyyy-MM-dd HH:mm:ss")  + "'";
		sql += " and '" + dtStaffEndDate.AddHours(-8).ToString("yyyy-MM-dd HH:mm:ss") + "')";

		inn.applySQL(sql);
	}

	//1.找到區間內的所有工時卡,計算每張工時卡的人力成本(工時*時薪)
	sql = "Update In_TimeRecord set in_cost = (in_work_hours*in_user_cost)";
	sql += " where (in_start_time between '" + dtStaffStartDate.AddHours(-8).ToString("yyyy-MM-dd HH:mm:ss")  + "'";
	sql += " and '" + dtStaffEndDate.AddHours(-8).ToString("yyyy-MM-dd HH:mm:ss") + "')";	
	inn.applySQL(sql);


	//將實際工時塞到各預算表內
	int intStepNo=0;



    /*以 職級-專案 作為分類單位,分別算出 職級-專案 的金額總計多少

    會得到:
    (專案1)
    TYLT-STR-PM:50萬
    TYLT-STR-QC:70萬
    CYLT-STR-QC:10萬
    CYLT-STR-DAM:10萬
    */


    //先清除本月份的全部人事實際			
	sql = "Update [In_Budget_Staff_Budget] set in_real_" + strMonth1 + "=0,in_timerecord_real_" + strMonth1 + "=0 where source_id in(select id from in_budget where state='Active')";
	inn.applySQL(sql);



    Item itmFakeStaffCosts = inn.newItem();
    Item itmFakeStaffCost=null;


    sql = "select (IN_RANK_NAME+ '-' + IN_PROJECT_NUMBER) as sys_c";
    sql += ",Round(sum(in_cost),0) as sys_cost";
    sql += ",sum(CAST(in_work_hours AS float)) as sys_hour";
    sql += " from in_timerecord  ";	
	sql += " where (in_start_time between '" + dtStaffStartDate.AddHours(-8).ToString("yyyy-MM-dd HH:mm:ss") + "' and '" + dtStaffEndDate.AddHours(-8).ToString("yyyy-MM-dd HH:mm:ss") + "')";	
    sql += " and state='Approved' ";
    //sql += " and in_project='A7D102594512473A802C188075BC3B55'";
    sql += " group by  (IN_RANK_NAME+ '-' + IN_PROJECT_NUMBER)";    
    Item itmGroupTimeRecs = inn.applySQL(sql);
    str_r +="預計要執行筆數::" + itmGroupTimeRecs.getItemCount() + "\n";
    Innosoft.InnUtility.AddLog("預計要執行筆數::" + itmGroupTimeRecs.getItemCount(),strMethodName);
    for(int i=0;i<itmGroupTimeRecs.getItemCount();i++)
    {
        intStepNo++;
        Item itmGroupTimeRec = itmGroupTimeRecs.getItemByIndex(i); 

        sql = "select distinct in_rank,in_company,in_project_company,in_project,in_project_keyed_name";
        sql += ",(IN_RANK_NAME+ '-' + IN_PROJECT_NUMBER)as sys_c from  in_timerecord";
        sql += " where (IN_RANK_NAME+ '-' + IN_PROJECT_NUMBER)='" + itmGroupTimeRec.getProperty("sys_c") + "'";
		str_r +=i.ToString() + ":" + itmGroupTimeRec.getProperty("sys_c") + "\n";
        Innosoft.InnUtility.AddLog(i.ToString() + ":" + itmGroupTimeRec.getProperty("sys_c"),strMethodName);
        Item itmTimeRec = inn.applySQL(sql);

        if(itmTimeRec.getItemCount()>1)
        {

            strErrorMessage = itmTimeRec.getItemByIndex(0).getProperty("sys_c") + "-類型的工時卡有一筆以上打卡公司或專案公司";
			str_r += strErrorMessage + "\n";
			Innosoft.InnUtility.AddLog(strErrorMessage,strMethodName);
			continue;

        }
        if(itmTimeRec.getResult()=="")
        {

            throw new Exception(itmGroupTimeRec.getProperty("sys_c") + "-類型的工時卡有0筆打卡公司或專案公司");
        }
        itmFakeStaffCost  = inn.newItem("FakeStaffCost");
        itmFakeStaffCost.setProperty("sys_c",itmTimeRec.getProperty("sys_c"));
        itmFakeStaffCost.setProperty("in_rank",itmTimeRec.getProperty("in_rank"));
        itmFakeStaffCost.setProperty("in_company",itmTimeRec.getProperty("in_company"));
        itmFakeStaffCost.setProperty("in_project_company",itmTimeRec.getProperty("in_project_company"));            
        itmFakeStaffCost.setProperty("in_project",itmTimeRec.getProperty("in_project"));
        itmFakeStaffCost.setProperty("in_project_keyed_name",itmTimeRec.getProperty("in_project_keyed_name"));
        double sys_cost = Convert.ToDouble(itmGroupTimeRec.getProperty("sys_cost","0"));
        sys_cost = Math.Round(sys_cost,0, MidpointRounding.AwayFromZero);
        itmFakeStaffCost.setProperty("sys_cost",sys_cost.ToString());  
        itmFakeStaffCost.setProperty("sys_hour",itmGroupTimeRec.getProperty("sys_hour")); 


         //Merge人事預算頁簽中各月的實際工時與費用

        aml = "<AML>";
        aml += "<Item type='In_Budget' action='get'>";
		aml += "<in_year>" +  dtStaffEndDate.ToString("yyyy") + "</in_year>";
        aml += "<in_refproject>" + itmFakeStaffCost.getProperty("in_project") + "</in_refproject>";
        aml += "</Item>";
        aml += "</AML>";
        Item itmBudget = inn.applyAML(aml);
		if(itmBudget.isError())
		{
			aml = "";
			sql = "";
			throw new Exception("查無[" + itmFakeStaffCost.getProperty("in_project_keyed_name") + "]對應的預算表");
		}

        //人事預算頁簽的實際費用與工時  

		//先檢查同公司別的 salary 會科對應的 Part 必須存在
		aml = "<AML>";
		aml += "<Item type='In_Accounting_items' action='get'>";
		aml += "<in_company>" + itmFakeStaffCost.getProperty("in_company") + "</in_company>";
        aml += "<in_accounting_number>salary</in_accounting_number>";
        aml += "</Item>";
		aml += "</AML>";		
		Item itmSalaryAccountingItem = inn.applyAML(aml);
		if(itmSalaryAccountingItem.isError())
		{
			throw new Exception("無法取得公司別為[" + itmFakeStaffCost.getProperty("in_company") + "]的salary 會計科目");
		}

		aml = "<AML>";
		aml += "<Item type='Part' action='get'>";
		aml += "<in_company>" + itmFakeStaffCost.getProperty("in_company") + "</in_company>";
        aml += "<in_accounting>" + itmSalaryAccountingItem.getID() + "</in_accounting>";
        aml += "</Item>";
		aml += "</AML>";		
		Item itmSalaryPart = inn.applyAML(aml);
		if(itmSalaryPart.isError())
		{
			throw new Exception("無法取得公司別為[" + itmFakeStaffCost.getProperty("in_company") + "]的salary Part");
		}



        aml = "<AML>";
        aml += "<Item type='In_Budget_Staff_Budget' action='merge' where=\"[source_id]='" + itmBudget.getID() + "' and [in_rank]='" + itmFakeStaffCost.getProperty("in_rank") + "'\">";
        aml += "<source_id>";
        aml += itmBudget.getID();
        aml += "</source_id>";
        aml += "<in_real_" + strMonth1 + ">" + itmFakeStaffCost.getProperty("sys_cost") + "</in_real_" + strMonth1 + ">";
        aml += "<in_timerecord_real_" + strMonth1 + ">" + itmFakeStaffCost.getProperty("sys_hour") + "</in_timerecord_real_" + strMonth1 + ">";
        aml += "<in_rank>" + itmFakeStaffCost.getProperty("in_rank") + "</in_rank>";
        aml += "<related_id>";
		aml += itmSalaryPart.getID() ;
        aml += "</related_id>";
        aml += "</Item></AML>";        
        Item itmStaffBudget = inn.applyAML(aml);
    }

	//C.更新各專案的表頭總工時欄位
	string strTotaltime = "";
	//DateTime dtNowYear = DateTime.Now;
	string strNowYear = dtStaffEndDate.ToString("yyyy");

    //1.找出最新的預算表
    aml = "<AML>";
    aml += "<Item type='In_Budget' action='get'>";
	aml += "<in_year>"+strNowYear+"</in_year>";
	aml += "</Item></AML>";
    Item itmBudgets = inn.applyAML(aml);
    for(int i=0;i<itmBudgets.getItemCount();i++){
        Item itmBudget = itmBudgets.getItemByIndex(i);

        sql = "SELECT SUM(in_timerecord_real_total) AS [TotalTime] FROM [IN_BUDGET_STAFF_BUDGET] WHERE [source_id] = '" + itmBudget.getID() + "'";
        Item itmBudget_Staff_Budget = inn.applySQL(sql);

        strTotaltime = "0";
        if(itmBudget_Staff_Budget.getResult()!=""){
            strTotaltime = itmBudget_Staff_Budget.getProperty("totaltime","0");
        }

        aml = "<AML>";
        aml += "<Item type='Project' action='get'>";
        aml += "<id>"+itmBudget.getProperty("in_refproject","")+"</id>";
        aml += "</Item></AML>";
        Item itmProject = inn.applyAML(aml);

        if(!itmProject.isError()){
            sql = "UPDATE [PROJECT] SET [in_hour_sum_act] = '" + strTotaltime + "' WHERE [ID] = '" + itmProject.getID() + "'";
            itmR = inn.applySQL(sql);
    }

	itmR = inn.newResult(str_r);
}







}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

	string strError = ex.Message + "\n";
	strError += (ex.InnerException==null?"":ex.InnerException.Message + "\n");

	if(aml!="")
	{
	strError += "無法執行AML:" + aml  + "\n";;
	}
	if(sql!="")
	{
		strError += "無法執行SQL:" + sql  + "\n";;
	}	
	string strErrorDetail="";
	strErrorDetail = strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
	//return inn.newError(strError);
	throw new Exception(strError);
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;

		
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Calculate_TimeRecordCost' and [Method].is_current='1'">
<config_id>77AAA3390BB2405E8FD8431133BA3E57</config_id>
<name>In_Calculate_TimeRecordCost</name>
<comments>計算本月的工時卡的人力成本</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
