var currentStylesheetId = this.getID();

var stylesheets = this.getInnovator().newItem("tp_Stylesheet", "get");
stylesheets.setProperty("parent_stylesheet", currentStylesheetId);
stylesheets = stylesheets.apply();

if (stylesheets.isError() || stylesheets.getItemCount() == 0) {
    return this;
}

for (var i = 0; i < stylesheets.getItemCount(); i++) {
    var stylesheet = stylesheets.getItemByIndex(i);
    stylesheet.setAction("delete");
    stylesheet.apply();
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='tp_StylesheetOnDelete' and [Method].is_current='1'">
<config_id>DABFB6C7E0864DC0AB901F0B32470702</config_id>
<name>tp_StylesheetOnDelete</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
