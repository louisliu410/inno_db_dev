//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();
string MeetingId = this.getProperty("in_meetingid");
string flag = "";
string MeetingUserName = "";
string ResponseTime = "";
string MeetingUserAns = "";
string MeetingMaster = "";
string BackupEmail="";
string CurrentTime="";
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string OuterInnovatorURL = _InnH.GetInVariable("base").getProperty("iplm_url", "");
/*
如果參數是in_meeting_userid和ConfirmAction
那麼就在MeetingUser.apply()後把ConfirmAction的值複製到flag上
以便讓notify_message發生作用
*/
if (MeetingId == null)//
{

    string MeetingUserId = this.getProperty("in_meeting_userid", "");
    string ConfirmAction = this.getProperty("ConfirmAction", "");
    Item ItmMeetingUser = inn.getItemById("In_Meeting_User", MeetingUserId);
    BackupEmail = ItmMeetingUser.getProperty("in_mail","");
    DateTime CurrentTimes = System.DateTime.Now;
    CurrentTime = CurrentTimes.ToString("yyyyMMddHHmmssfff");
   
  
    Item MeetingUser = inn.newItem("In_Meeting_User", "edit");
    MeetingUser.setAttribute("where", "[In_Meeting_User].id='" + MeetingUserId + "'");
    switch (ConfirmAction)
    {
        case "nogo":
            MeetingUser.setProperty("in_ans", "reject");
            MeetingUser.setProperty("in_note_state", "cancel");
            MeetingUser.setProperty("in_note",BackupEmail);
            MeetingUser.setProperty("in_mail",CurrentTime);
            break;
        case "go":
            MeetingUser.setProperty("in_ans", "agree");
            break;
        default:

            break;
    }
    MeetingUser = MeetingUser.apply();
    flag = ConfirmAction;
    MeetingId = MeetingUser.getProperty("source_id", "");
    /******************************************************************************/
    //這裡的變數為In_Meeting_User上的Property並對應到Mail中的欄位
    MeetingUserName = MeetingUser.getProperty("in_name", "");
    DateTime TodayTime = System.DateTime.Now;
    ResponseTime = TodayTime.ToString("yyyy/MM/dd HH:mm");//取系統時間做為建立時間
    /******************************************************************************/
    //這裡不直接用switch case做英文轉中文
    /*
     <AML>
	    <Item type="List" action="get">
		    <name>in_meeting_ans</name>
		    <Relationships>
			    <Item type="Value" action="get">
			    </Item>
		    </Relationships>
	    </Item>
    </AML>
    */
    //以上AML可知agree,reject,null為物件類型Value上的Property,如<value>agree</value>
    //而對應的中文字同在物件類型Value上的Property,如<label>要參加</label>
    MeetingUserAns = MeetingUser.getProperty("in_ans", "");
    Item itmList = inn.newItem("List", "get");
    itmList.setProperty("name", "in_meeting_ans");
    itmList = itmList.apply();
    string ListId = itmList.getID();
    Item itmValue = newItem("Value", "get");
    itmValue.setProperty("source_id", ListId);
    itmValue.setProperty("value", MeetingUserAns);
    itmValue = itmValue.apply("get");
	if(!itmValue.isError())
		MeetingUserAns = itmValue.getProperty("label");//應該要是"要參加,不參加,沒回覆其一"
    /******************************************************************************/

}
//此處挖到relationship的理由是為了讓In_Meeting_info上有資料
Item Meeting = inn.getItemById("In_Meeting", MeetingId);
Item MeetingRelated = Meeting.fetchRelationships("In_Meeting_Agenda");
Item MeetingRelationships = MeetingRelated.getRelationships();
Meeting = Meeting.apply("get");
string in_title = Meeting.getProperty("in_title", "");

if (flag != "")//此處是對應in_meeting_userid和ConfirmAction的作法
{
    switch (flag)
    {
        case "nogo":
            Meeting.setProperty("notify_message", "已取消報名,您可以至會議列表中重新選擇其他場次");
            break;
        case "go":
            Meeting.setProperty("notify_message", "已確認參加,當天請準時出席");
            break;
        default:

            break;
    }
    string MeetingTitle = Meeting.getProperty("in_title","");//會議名稱
    MeetingMaster = OuterInnovatorURL + "pages/c.aspx?method=In_Get_SingleItemInfoGeneral&itemtype=in_meeting&itemid=" + MeetingId + "&fetchproperty=4&mode=direct&page=in_Meeting_Master.html&inn_editor_type=edit";

    string in_contract = Meeting.getProperty("in_contract", "");//聯繫窗口
    string in_mail = Meeting.getProperty("in_mail", "");//聯繫郵件
    //這裡的str為"在報名者按下確定參加或取消報名後，寄送給系統管理員的通知"
    string str = @"
<html>
<body>
<table border=0 cellpadding=0 cellspacing=0 width=203 style='border-collapse:
collapse;table-layout:fixed;width:152pt'>
<h1>系統管理員通知</h1>
<table border=""1"" cellspacing=""0"" width=""250px"">
<tr>
<td style=""min-width:0px;"" width=""60"">姓名:</td>
<td>@MeetingUserName</td>
</tr>


<tr>
<td style=""min-width:0px;"" width=""60"">回復時間:</td>
<td>@ResponseTime</td>
</tr>

<tr>
<td style=""min-width:0px;"" width=""60"">會議:</td>
<td>@MeetingTitle</td>
</tr>

<tr>
<td style=""min-width:0px;"" width=""60"">回復意願</td>
<td>@MeetingAns</td>
</tr>

<tr>
<td style=""min-width:0px;"" width=""60"">報名列表:</td>
<td><a href=""@MeetingMaster"">請點選此連結</a></td>
</tr>
</table>
</table>
</body>
</html>"
               .Replace("@MeetingUserName", MeetingUserName)
               .Replace("@ResponseTime", ResponseTime)
               .Replace("@MeetingTitle", MeetingTitle)
               .Replace("@MeetingAns", MeetingUserAns)
               .Replace("@MeetingMaster", MeetingMaster)
               ;
 

    System.Net.Mail.MailAddress from = new System.Net.Mail.MailAddress(in_mail, in_contract);
    System.Net.Mail.MailAddress to = new System.Net.Mail.MailAddress(in_mail, in_contract);
    System.Net.Mail.MailMessage myMail = new System.Net.Mail.MailMessage(from, to);
    myMail.Subject = "[學員意願回覆]-" + MeetingTitle;
    myMail.SubjectEncoding = System.Text.Encoding.UTF8;
    myMail.Body = str;
    myMail.BodyEncoding = System.Text.Encoding.UTF8;
    myMail.IsBodyHtml = true;
    CCO.Email.SetupSmtpMailServerAndSend(myMail);
}


return Meeting;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_MeetingUserResponse' and [Method].is_current='1'">
<config_id>625BA4B54B3A4EA38AE5E6091131DAEF</config_id>
<name>In_MeetingUserResponse</name>
<comments>在報名者按下確定參加或取消報名後，寄送給系統管理員的通知</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
