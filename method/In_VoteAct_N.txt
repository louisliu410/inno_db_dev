/*
目的:接收Act Vote
做法:
*/

string strMethodName = "In_VoteAct_N";
//System.Diagnostics.Debugger.Break();
// Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
//     bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";



_InnH.AddLog(strMethodName,"MethodSteps");
Item itmR = this;

/*
<AML>
	<Item type='Activity' action='EvaluateActivity'>
		<Activity>#actid</Activity>
		<ActivityAssignment>#actassid</ActivityAssignment>
		<Paths>
			<Path id='#pathid'>#pathlabel</Path>
		</Paths>
		<DelegateTo>0</DelegateTo>
		<Tasks>
			<Task id='taskid1' completed='1' />
			<Task id='taskid2' completed='1' />
			<Task id='taskid3' completed='1' />
		</Tasks>
		<Variables />
		<Authentication mode='' />
		<Comments>#comments</Comments>
		<Complete>1</Complete>
	</Item>
</AML>


<AML>
	<Item>
		<actid/>
		<actassid/>
		<pathid/>
		<pathlabel/>
		<task_count />
		<task_id_1/>
		<task_complete_1/>
		<task_id_2/>
		<task_complete_2/>
		<task_id_3/>
		<task_complete_3/>
		<comments />
		<complete />
	</Item>
</AML>

*/


try
{
    //要移除 <?xml version="1.0" encoding="utf-8"?>
    if (this.dom.FirstChild.NodeType == XmlNodeType.XmlDeclaration)
    {
      this.dom.RemoveChild( this.dom.FirstChild);
    }


   //System.Diagnostics.Debugger.Break();

string fileIDcsv=this.getProperty("fileids","");
string strConfigId=this.getProperty("in_config_id","");
string strItemId=this.getProperty("in_item_id","");
string strItemType = this.getProperty("itemtype","");

if(fileIDcsv!="" && strConfigId!=""){
	//先判斷 itemtype_File 物件是否存在,若存在才要加
	Item itmRel = inn.newItem("RelationshipType","get");
	itmRel.setProperty("name",strItemType + "_File");
	itmRel = itmRel.apply();
	if(!itmRel.isError())
	{
		if(strItemId=="")
		{
			Item itmSourceItem = _InnH.GetCurrentItemByConfigId(strItemType,strConfigId);
			strItemId = itmSourceItem.getID();
		}
		
		//要加檔案,大部分的關聯都是 use_src_access,會繼承 add 權限,因此直接換身分比較快
		Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
		bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);
		string[] files=fileIDcsv.Split(',');
		for(int fileCount=0;fileCount<files.Length;fileCount++){
			Item singleFile=inn.newItem(strItemType + "_File","add");
			singleFile.setProperty("source_id",strItemId);
			singleFile.setProperty("related_id",files[fileCount]);
			string note=this.getProperty("in_upload_file_"+files[fileCount],"");    
			singleFile.setProperty("in_note",note);
			singleFile.apply();
		}
		if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
	}
}



	string actid = this.getProperty("actid","");
	string actassid = this.getProperty("actassid","");
	string pathid = this.getProperty("pathid","");
	string pathlabel = this.getProperty("pathlabel","");
	string task_ids = this.getProperty("task_ids","");
	string[] arrTaskIds = task_ids.Split(',');
	Dictionary<string, string> dicTasks = new Dictionary<string, string>( );
	
	if(task_ids!="")
	{
    	for(int i=0;i<arrTaskIds.Length;i++)
    	{
    		dicTasks.Add(this.getProperty("task_id_" + arrTaskIds[i],""),this.getProperty("task_complete_" + arrTaskIds[i],"0"));
    	}
	}
	string comments = this.getProperty("comments","");
	string complete = this.getProperty("complete","0");

	aml = "<AML>";
	aml += "	<Item type='Activity' action='EvaluateActivity'>";
	aml += "		<Activity>" + actid + "</Activity>";
	aml += "		<ActivityAssignment>" + actassid + "</ActivityAssignment>";
	aml += "		<Paths>";
	aml += "			<Path id='" + pathid + "'>" + pathlabel + "</Path>";
	aml += "		</Paths>";
	aml += "		<DelegateTo>0</DelegateTo>";
	aml += "		<Tasks>";
	foreach ( var task in dicTasks )
    {
		aml += "<Task id='" + task.Key + "' completed='" + task.Value + "' />";
    }
	aml += "		</Tasks>";
	aml += "		<Variables />";
	aml += "		<Authentication mode='' />";
	aml += "		<Comments>" + comments + "</Comments>";
	aml += "		<Complete>" + complete + "</Complete>";
	aml += "	</Item>";
	aml += "</AML>";

	Item VoteResult = inn.applyAML(aml);
	//若簽審成功,result是一片空白

	Item itmResponse = inn.newItem("response");
	if(VoteResult.isError())
	{
		itmResponse.setProperty("status","error");
		itmResponse.setProperty("message",VoteResult.getErrorString());
	}
	else
	{
		itmResponse.setProperty("status","success");
		itmResponse.setProperty("message","");
	}
	//aml = "<AML>";
	//aml += "<Item type='Document' action='get' select='id' />";
	//aml += "</AML>";
	//itmR = inn.applyAML(aml);
	itmR.appendItem(itmResponse);



}
catch(Exception ex)
{	
// 	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);



	string strError = ex.Message + "\n";


	string strErrorDetail="";
	//strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	_InnH.AddLog(strErrorDetail,"Error");
	strError = strError.Replace("</br>","");
	strError = strError.Replace("<br/>","");
	throw new Exception(_InnH.Translate(strError));
}
// if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_VoteAct_N' and [Method].is_current='1'">
<config_id>7D0A56DEDE39458CB49278E1A16589EA</config_id>
<name>In_VoteAct_N</name>
<comments>接收Act Vote</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
