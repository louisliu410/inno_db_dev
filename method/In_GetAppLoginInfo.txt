/*
目的:提供APP本次登入的基本資訊
做法:
1.取得User物件
2.提供目前登入者所屬 identity 的 name list
3.提供目前登入者的 user identity 資訊,以便作為 client cache default 之用
*/

Innovator inn = this.getInnovator();
string login_name = this.getProperty("login_name","");
Item AppInfo = inn.newItem("User","get");
AppInfo.setProperty("login_name",login_name);
AppInfo=AppInfo.apply();

Item identity=inn.newItem("Identity","get");
identity.setAttribute("select","name");
identity.setProperty("id",Aras.Server.Security.Permissions.Current.IdentitiesList);
identity.setPropertyCondition("id","in");
identity=identity.apply();
List<string> names=new List<string>();
for(int i=0;i<identity.getItemCount();i++){
	names.Add(identity.getItemByIndex(i).getProperty("name"));
}
AppInfo.setProperty("identity_list",string.Join(",",names.ToArray()));


return AppInfo;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_GetAppLoginInfo' and [Method].is_current='1'">
<config_id>C21830282ADA4D9F9DADAE0996E09AD2</config_id>
<name>In_GetAppLoginInfo</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
