PublishingDialog = function() {
	var topWindow = aras.getMostTopWindowWithAras(window);

	this.dialogArguments = parent.frameElement.dialogArguments;
	this.documentItem = this.dialogArguments.documentItem;
	this.settings = {filter: '{}', language: 'en', conversionType: 'xml'};
	this.formControls = {};
	this.optionFamilies = null;
	this.pathModule = topWindow.Path;

	this.initializeControls();
};

PublishingDialog.prototype.initializeControls = function() {
	var serverResponce;

	// buttons section
	this.formControls.okButton = document.getElementById(document.fieldsTab['convert_button']);
	this.formControls.okButton.addEventListener('click', function() { this.convertDocument(this.documentItem); }.bind(this));

	this.formControls.closeButton = document.getElementById(document.fieldsTab['close_button']);
	this.formControls.closeButton.addEventListener('click', this.Close.bind(this));

	this.formControls.filterButton = document.getElementById(document.fieldsTab['filter_button']);
	this.formControls.filterButton.addEventListener('click', this.showFiltersDialog.bind(this));

	this.formControls.clearFilterButton = document.getElementById(document.fieldsTab['clear_filter_button']);
	this.formControls.clearFilterButton.addEventListener('click', function() { this.setFilterCondition(''); }.bind(this));

	// input section
	this.formControls.filterTextArea = document.querySelector('[id="' + document.fieldsTab['filter_condition'] + '"] textarea');
	this.formControls.fileExtensionInput = document.querySelector('[id="' + document.fieldsTab['file_extension_input'] + '"] input');

	// fill conversion formats
	this.formControls.coversionTypeList = document.querySelector('[id="' + document.fieldsTab['conversion_format_list'] + '"] select');
	this.fillSelectFromList(this.formControls.coversionTypeList, 'tp_PublishingConversionFormat');
	this.formControls.coversionTypeList.addEventListener('change', this.ConversionTypeChanged.bind(this), false);
	this.formControls.fileExtensionInput.value = this.getConversionType();

	// fill conversion results
	this.formControls.coversionResultList = document.querySelector('[id="' + document.fieldsTab['conversion_result_list'] + '"] select');
	this.fillSelectFromList(this.formControls.coversionResultList, 'tp_PublishingConversionResult');

	// fill linked document allocation types
	this.formControls.documentAllocationList = document.querySelector('[id="' + document.fieldsTab['linked_documents_list'] + '"] select');
	this.fillSelectFromList(this.formControls.documentAllocationList, 'tp_PublishingDocumentAllocation');

	// fill resource allocation types
	this.formControls.resourceAllocationList = document.querySelector('[id="' + document.fieldsTab['resource_allocation_list'] + '"] select');
	this.fillSelectFromList(this.formControls.resourceAllocationList, 'tp_PublishingResourceAllocation');

	// fill language select
	this.formControls.languageList = document.querySelector('[id="' + document.fieldsTab['language_list'] + '"] select');
	serverResponce = aras.soapSend('ApplyItem', '<Item type="Language" action="get" select="name,code"></Item>');

	if (!serverResponce.isFault()) {
		var systemLanguages = serverResponce.getResult().selectNodes('Item');
		var languageName;
		var languageCode;
		var itemNode;
		var i;

		for (i = 0; i < systemLanguages.length; i++) {
			itemNode = systemLanguages[i];
			languageName = aras.getItemProperty(itemNode, 'name');
			languageCode = aras.getItemProperty(itemNode, 'code');

			this.formControls.languageList.options[i] = new Option(languageName, languageCode);
		}

		this.formControls.languageList.selectedIndex = 0;
		aras.updateDomSelectLabel(this.formControls.languageList);
	} else {
		aras.AlertError(serverResponce);
	}
};

/**
* @param {event} changeEvent
*/
PublishingDialog.prototype.ConversionTypeChanged = function(changeEvent) {
	var conversionType = this.getConversionType();
	this.formControls.fileExtensionInput.value = conversionType;
	if (conversionType === 'pdf') {
		this.formControls.resourceAllocationList.parentNode.classList.add('sys_f_div_select_disabled');
		this.formControls.resourceAllocationList.disabled = true;
	} else {
		this.formControls.resourceAllocationList.parentNode.classList.remove('sys_f_div_select_disabled');
		this.formControls.resourceAllocationList.disabled = false;
	}
};

/**
* @param {SelectControl} selectControl
* @param {String} listName
*/
PublishingDialog.prototype.fillSelectFromList = function(selectControl, listName) {
	var listItem = aras.getItemByName('List', listName);

	if (listItem) {
		var listValueNodes = listItem.selectNodes('Relationships/Item[@type="Value"]');
		var itemNode;

		for (i = 0; i < listValueNodes.length; i++) {
			itemNode = listValueNodes[i];
			selectControl.options[i] = new Option(aras.getItemProperty(itemNode, 'label'), aras.getItemProperty(itemNode, 'value'));
		}

		selectControl.selectedIndex = 0;
		aras.updateDomSelectLabel(selectControl);
	}
};

PublishingDialog.prototype.getConversionType = function() {
	var selectControl = this.formControls.coversionTypeList;

	return selectControl.selectedIndex != -1 ? selectControl.options[selectControl.selectedIndex].value : 'xml';
};

PublishingDialog.prototype.getConversionResult = function() {
	var selectControl = this.formControls.coversionResultList;

	return selectControl.selectedIndex != -1 ? selectControl.options[selectControl.selectedIndex].value : 'window';
};

PublishingDialog.prototype.getDocumentAllocation = function() {
	var selectControl = this.formControls.documentAllocationList;

	return selectControl.selectedIndex != -1 ? selectControl.options[selectControl.selectedIndex].value : 'join';
};

PublishingDialog.prototype.getResourceAllocation = function() {
	var selectControl = this.formControls.resourceAllocationList;

	return selectControl.selectedIndex != -1 ? selectControl.options[selectControl.selectedIndex].value : 'source';
};

PublishingDialog.prototype.getSelectedLanguage = function() {
	var languageSelect = this.formControls.languageList;

	return languageSelect.selectedIndex != -1 ? languageSelect.options[languageSelect.selectedIndex].value : '';
};

PublishingDialog.prototype.getFileExtension = function() {
	var languageSelect = this.formControls.languageList;

	return this.formControls.fileExtensionInput.value;
};

PublishingDialog.prototype.getFilterCondition = function() {
	return this.settings.filter;
};

/**
* @param {String} filterCondition
*/
PublishingDialog.prototype.setFilterCondition = function(filterCondition) {
	this.settings.filter = filterCondition || '{}';
	this.formControls.filterTextArea.value = (this.settings.filter !== '{}') ?
		filterCondition.replace(/\]\,/g, '\n').replace(/[\[\]\{\}\"]/g, '').replace(/\:/g, ' - ') : '';
};

/**
* @param {Item} targetDocumentItem
*/
PublishingDialog.prototype.convertDocument = function(targetDocumentItem) {
	var spinner = parent.document.getElementById('dimmer_spinner');
	spinner.style.display = 'block';

	var documentItem = aras.newIOMItem();
	var conversionType = this.getConversionType();
	var conversionRuleName;
	var createTaskResult;
	var taskId;
	var mainWindow = aras.getMainWindow();

	switch (conversionType) {
		case 'xml':
			conversionRuleName = 'tp_XmlPublishingRule';
			break;
		case 'pdf':
			var consumeLicenseResult = mainWindow.aras.ConsumeLicense('Aras.HTMLtoPDFConverter');
			if (consumeLicenseResult.isError) {
				if (consumeLicenseResult.errorMessage.indexOf('FeatureHasNoLicensesException') > -1 ||
					consumeLicenseResult.errorMessage.indexOf('FeatureLicenseValidationException') > -1) {
					aras.AlertWarning(aras.getResource('../Modules/aras.innovator.TDF', 'helper.feature_has_no_license', 'Aras.HTMLtoPDFConverter'));
				} else {
					aras.AlertError(consumeLicenseResult.errorMessage);
				}
				spinner.style.display = 'none';
				return;
			}
			conversionRuleName = 'tp_PdfPublishingRule';
			break;
		case 'html':
			conversionRuleName = 'tp_HtmlPublishingRule';
			break;
		default:
			aras.AlertError(aras.getResource('../Modules/aras.innovator.TDF', 'includes.conversiontypenotsupported'));
			spinner.style.display = 'none';
			return;
	}

	// create Conversion task with appropriate CoversionRule
	documentItem.loadAML(targetDocumentItem.node.xml);
	documentItem.setProperty('publishing_language', this.getSelectedLanguage());
	documentItem.setProperty('publishing_filters', this.getFilterCondition());
	documentItem.setProperty('publishing_rule_name', conversionRuleName);
	documentItem.setProperty('publishing_file_extension', this.getFileExtension());
	documentItem.setProperty('publishing_document_allocation', this.getDocumentAllocation());
	documentItem.setProperty('publishing_resource_allocation', conversionType === 'pdf' ? 'source' : this.getResourceAllocation());

	createTaskResult = documentItem.apply('tp_CreatePublishingTask');
	taskId = createTaskResult.getResult();

	// if task was created
	if (taskId) {
		var targetWindow = window === mainWindow ? mainWindow.main : window;
		var self = this;
		var dialogParameters = {
			dialogWidth: 300, dialogHeight: 160, resizable: false,
			aras: aras,
			itemType: 'ConversionTask',
			itemId: taskId,
			propertyName: 'status',
			propertyState: ['Succeeded', 'Failed', 'Discarded'],
			title: aras.getResource('../Modules/aras.innovator.TDF', 'includes.waitfortaskcompletion'),
			timeout: 300000, //5 min
			content: 'waitPropertyStatusDialog.html',
			hideSpinner: function() {
				spinner.style.display = 'none';
			}
		};

		targetWindow.ArasModules.Dialog.show('iframe', dialogParameters).promise.then(function(taskState) {
			if (taskState === 'Succeeded') {
				setTimeout(function() {
					self.processConversionResult(taskId);
				}, 0);
			} else if (taskState === '_timeout') {
				aras.AlertError(aras.getResource('../Modules/aras.innovator.TDF', 'publishing.timeout_reached'));
			}
		});
	}
};

PublishingDialog.prototype.showFiltersDialog = function() {
	var dialogParameters;
	var dialogOptions;
	var mainWindow;
	var targetWindow;

	// search allowed optionFamilies
	if (!this.optionFamilies) {
		var response = aras.soapSend('ApplyItem', '<Item type="tp_block" action="tp_GetOptionFamilies" id="' + this.documentItem.getID() + '" />');

		if (!response.isFault()) {
			this.optionFamilies = JSON.parse(response.getResult().text);
		} else {
			aras.AlertError(response);
			return;
		}
	}

	// if option families are exist for technical document, then show filters dialog
	mainWindow = aras.getMainWindow();
	targetWindow = window === mainWindow ? mainWindow.main : window;

	dialogParameters = {
		dialogWidth: 322, dialogHeight: 375,
		title: 'Select Document Filters',
		formId: '68962F55F96E4583AD53676C3BEF91EC', // tp_ChangeBlockCondition Form
		aras: aras,
		isEditMode: true,
		isDisabled: false,
		condition: JSON.parse(this.getFilterCondition()),
		externalcondition: {},
		optionFamilies: this.optionFamilies,
		content: 'ShowFormAsADialog.html'
	};

	this.dialogArguments.parentWindow.ArasModules.Dialog.show('iframe', dialogParameters).promise.then(function(result) {
		if (result) {
			this.setFilterCondition(JSON.stringify(result));
		}
	}.bind(this));
};

/**
* @param {String} taskId
*/
PublishingDialog.prototype.processConversionResult = function(taskId) {
	var conversionResult = aras.soapSend('ApplyItem', '<Item type="ConversionTaskResult" action="get" select="file_id"><source_id>' +
		taskId + '</source_id></Item>');

	if (!conversionResult.isFault()) {
		var resultItem = conversionResult.getResult().selectSingleNode('Item');
		var fileId = aras.getItemProperty(resultItem, 'file_id');
		var fileItem = aras.soapSend('ApplyItem', '<Item type="File" action="get" id="' + fileId + '"></Item>');

		if (!fileItem.isFault()) {
			var resultAction = this.getConversionResult();
			var fileNode = fileItem.getResult().selectSingleNode('Item');

			switch (resultAction) {
				case 'window':
					this.showConvertedFile(fileNode);
					break;
				case 'file':
					var documentName = this.documentItem.getProperty('name');

					this.downloadConvertedFile(fileNode, documentName);
					break;
			}
		}
	}
};

/**
* @param {ItemNode} fileNode
*/
PublishingDialog.prototype.showConvertedFile = function(fileNode) {
	if (fileNode) {
		aras.uiShowItemEx(fileNode, undefined);
	}
};

/**
* @param {ItemNode} fileNode
* @param {String} preferedFileName
*/
PublishingDialog.prototype.downloadConvertedFile = function(fileNode, preferedFileName) {
	if (fileNode) {
		var fileName = preferedFileName || aras.getItemProperty(fileNode, 'filename');
		var downloadResult = aras.downloadFile(fileNode);

		if (downloadResult) {
			aras.AlertSuccess(aras.getResource('', 'file_management.file_succesfully_downloaded', fileName), aras.getMostTopWindowWithAras(window));
		}
	}
};

PublishingDialog.prototype.Close = function() {
	this.dialogArguments.dialog.close();
};

aras.IomInnovator.ConsumeLicense('Aras.PublishingService');
// Create instance
PublishingDialog = new PublishingDialog();

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='tp_PublishingDialogAPI' and [Method].is_current='1'">
<config_id>C201ECA218214B07A0F663B087059B0D</config_id>
<name>tp_PublishingDialogAPI</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
