string userId = this.getID();
string aliasIdentityId = GetUserIsAliasIdentityId(userId, CCO);
List<string> ids = GetAllUserIdentities(aliasIdentityId, CCO);
return this.getInnovator().newResult(String.Join("|", ids.ToArray()));
}

private string GetUserIsAliasIdentityId(string userId, Aras.Server.Core.CallContext CCO)
{
	Item a = this.newItem("Alias", "get");
	a.setProperty("source_id", userId);
	Item identity = this.newItem("Identity", "get");
	identity.setProperty("is_alias", "1");
	a.setPropertyItem("related_id", identity);
	a = a.apply();
	if (a.isError())
	{
		throw new Exception(CCO.ErrorLookup.Lookup("SSVC_GetAllUserIdentitiesNoIsAliasIdentity"));
	}
	else if (a.getItemCount() > 1)
	{
		throw new Exception(CCO.ErrorLookup.Lookup("SSVC_GetAllUserIdentitiesMoreThanOneIsAliasIdentity"));
	}
	return a.getProperty("related_id");
}

private List<string> GetAllUserIdentities(string aliasIdentityId,  Aras.Server.Core.CallContext CCO)
{
	const string worldIdentityId = "A73B655731924CD0B027E4F4D5FCC0A9";
	Aras.Server.Core.InnovatorDatabase database = CCO.Variables.InnDatabase;
	List<string> identityMembers = Aras.Server.Security.Permissions.GetAncestorIdentityIds(database, new string[]{ aliasIdentityId}).ToList();
	identityMembers.Add(aliasIdentityId);
	identityMembers.Add(worldIdentityId);
	return identityMembers;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='VC_GetAllUserIdentities' and [Method].is_current='1'">
<config_id>F1ECC8450C8A491BB8009A829185D3E6</config_id>
<name>VC_GetAllUserIdentities</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
