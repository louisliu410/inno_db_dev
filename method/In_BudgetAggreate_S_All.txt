/*
目的:找出所有預算表,執行重整算出第一頁資料
做法:
*/

//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";


Item itmR = this;
string strNG="";
int intOk=0;	
int intAll=0;
string strQueueID = this.getProperty("queue_id","");
//strQueueID="2210DD7E425749D1A0307EACCEBB1771";
 string strResponse = "";
 Item itmQueue=null;
try
{
    if(strQueueID=="")
    {
        aml = "<AML>";
    	aml += "<Item type='In_Budget' action='get'>";
    	//aml += "<state condition='ne'>Closed</state>";
    	aml += "<state>Active</state>";
    	//aml += "<in_year>2016</in_year>";
    	//aml += "<id>5719773D89C54A0DB6A3776321DB9A82</id>";
    	aml += "</Item></AML>";
    }
    else
    {
        aml = "<AML>";
        aml += "<Item type='In_Queue' action='get' id='" + strQueueID + "'>";
        aml += "</Item></AML>";
        
         itmQueue = inn.applyAML(aml);
        string strParam = itmQueue.getProperty("in_inparam");
        Item itmParam = inn.newItem("param");
        itmParam.loadAML(strParam);
        
        string strIDs = itmParam.getProperty("ids");
        strResponse = itmParam.getProperty("response");
         
       _InnH.ChangeQueueState(itmQueue, "In_Process");

        aml = "<AML>";
    	aml += "<Item type='In_Budget' action='get' idlist='" + strIDs + "'>";
    	aml += "<state>Active</state>";
    	aml += "</Item></AML>";
    
    }
    
    
	
	Item itmBudgets = inn.applyAML(aml);
	intAll = itmBudgets.getItemCount();
	for(int i=0;i<itmBudgets.getItemCount();i++)
	{
		Item itmBudget = itmBudgets.getItemByIndex(i);
		try{
		    //若沒指定 yearmonth 假屬性,則以執行的當月份為計算基礎
		    string strYearMonth = this.getProperty("yearmonth",System.DateTime.Now.AddMonths(-1).ToString("yyyy-MM"));
		    strYearMonth = "2017-02";
		    itmBudget.setProperty("yearmonth",strYearMonth);
			itmBudget = itmBudget.apply("In_BudgetAggreate_S");	
			intOk +=1;
			Innosoft.InnUtility.AddLog(intOk + ":" + itmBudget.getProperty("item_number") + "-" + itmBudget.getProperty("in_year"),"In_BudgetAggreate_S_All");
		}
		catch(Exception ex1)
		{
			strNG += itmBudget.getProperty("item_number") + "-" + itmBudget.getProperty("in_year") + ":" + ex1.Message + "\n";
			Innosoft.InnUtility.AddLog("NG:" + itmBudget.getProperty("item_number") + "-" + itmBudget.getProperty("in_year") + ":" + ex1.Message ,"In_BudgetAggreate_S_All");
		}
	}

}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);


	string strMethodName ="In_BudgetAggreate_S_All";
	strMethodName = strMethodName.Replace("Aras.IOM.Item methodCode(Aras.Server.Core.","");
	strMethodName = strMethodName.Replace("EventArgs)","");
	string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;


	string strError = ex.Message + "\n";
	//strError += (ex.InnerException==null?"":ex.InnerException.Message + "\n");

	if(aml!="")
    	strError += "無法執行AML:" + aml  + "\n";

	if(sql!="")
		strError += "無法執行SQL:" + sql  + "\n";

	string strErrorDetail="";
	strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
	//return inn.newError(_InnH.Translate(strError));
	//throw new Exception(_InnH.Translate(strError));
	strNG = strError;
    
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);


string strOk = "";
strOk = "分批處理:" + strResponse + "\n";
strOk += "總共:" +  intAll.ToString() + "筆\n";
strOk += "成功:" + intOk.ToString() + "筆\n";
strOk += "失敗說明:" + strNG+ "\n";

if(strNG!="")
{
	Innosoft.InnUtility.AddLog(strNG,"預算表重整錯誤-" + System.DateTime.Now.ToString("yyyy"));
	string strErrorSubject = "預算表重整錯誤-" + System.DateTime.Now.ToString("yyyy");
	string setErrorBody = strNG.Replace("\n","<br>");
	if(itmQueue!=null)
    {
        //執行結果,結束狀態,email message HTML 內容,錯誤訊息的 HTML 內容,會主動發給 Innovator Admin
	    _InnH.CloseQueue(itmQueue,strOk,"Failed",strOk,strOk);
    }
    else
	    _InnH.SendEmailToAdmin(strErrorSubject,setErrorBody);
	
}
else
{
    Innosoft.InnUtility.AddLog(strOk,"預算表重整-" + System.DateTime.Now.ToString("yyyy"));
    if(itmQueue!=null)
    {
        _InnH.CloseQueue(itmQueue,strOk,"Closed");
    }	
   
    
}




return inn.newResult(strOk);
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_BudgetAggreate_S_All' and [Method].is_current='1'">
<config_id>4FC89DB47FFB436597FA7BE4576B0AC1</config_id>
<name>In_BudgetAggreate_S_All</name>
<comments>找出所有預算表,執行重整算出第一頁資料</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
