' name: SQL onAfterAdd
' created: 5-OCT-2005 George J. Carrette
'MethodTemplateName=VBScriptMain;

Sub Main(inDom As XmlDocument,outDom As XmlDocument,Iob as Object,Svr As Object,Ses As Object)
 cco.xml.setitemproperty(cco.xml.GetRequestItem(indom),"PROCESS","onAfterAdd")
 cco.applyitem.ExecuteMethodByName(indom,outdom,"SQL PROCESS",True)
End Sub

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='SQL onAfterAdd' and [Method].is_current='1'">
<config_id>3D1554C01DCB4A6092C57555C67648A0</config_id>
<name>SQL onAfterAdd</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
