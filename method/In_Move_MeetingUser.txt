//這個方法只負責搬移物件。任何元會議的相關處理應該在進來之前處理完成。
//System.Diagnostics.Debugger.Break();
Innovator inn=this.getInnovator();
string tgt_meeting=this.getProperty("tgt_meeting","no_data");
Item meetingUser=this.getPropertyItem("meeting_user");
string muid;
string message="";

string action="add";
//檢查傳入的使用者是否已存在(可能是多重註冊或口袋名單，是的話merge)
Item checkExist=inn.newItem();
checkExist.setAttribute("type","Method");
checkExist.setAttribute("action","In_CheckMeetingUser");
checkExist.setProperty("meeting_id",tgt_meeting);
checkExist.setProperty("email",meetingUser.getProperty("in_mail"));
checkExist=checkExist.apply();
muid=checkExist.getResult();


Item applicant=inn.newItem();

if(muid!=""){
    //是的話將動作改成merge
    action="merge";
    applicant.setAttribute("id",muid);
}

applicant.setProperty("source_id",tgt_meeting);
applicant.setProperty("in_name",meetingUser.getProperty("in_name"));
applicant.setProperty("in_tel",meetingUser.getProperty("in_tel"));
applicant.setProperty("in_mail",meetingUser.getProperty("in_mail"));
applicant.setProperty("in_refuser",meetingUser.getProperty("in_refuser"));
applicant.setAttribute("action",action);
applicant.setAttribute("type","In_Meeting_User");
applicant.setProperty("in_regdate",meetingUser.getProperty("in_regdate"));
applicant.setProperty("in_allowed","1");

applicant=applicant.apply();

//取得與這個使用者關聯的所有問卷結果。
Item surveys=inn.newItem();
surveys.setAttribute("type","In_Meeting_Surveys_result");
surveys.setAttribute("action","get");
surveys.setProperty("in_participant",meetingUser.getID());
surveys.setProperty("source_id",meetingUser.getProperty("source_id"));
surveys=surveys.apply();


//檢查是否已填過(多次改換場次，若已填過直接break)

    int xCount=surveys.getItemCount();
    for(int j=0;j<xCount;j++){
        Item moving=surveys.getItemByIndex(j);

        Item answerSheetDD=inn.newItem();
        answerSheetDD.setAttribute("type","In_Meeting_Surveys_result");
        answerSheetDD.setAttribute("action","add");
        answerSheetDD.setProperty("source_id",tgt_meeting);
        answerSheetDD.setProperty("related_id",moving.getProperty("related_id"));
        answerSheetDD.setProperty("in_answer",moving.getProperty("in_answer"));
        answerSheetDD.setProperty("in_surveytype",moving.getProperty("in_surveytype"));
        answerSheetDD=answerSheetDD.apply();

        //如果不是錯誤也不是空的就直接break整個迴圈。
        if(!answerSheetDD.isError() && !answerSheetDD.isEmpty()){
            break;
        }
        Item answerSheet=inn.newItem();
    
        answerSheet.setAttribute("type","In_Meeting_Surveys_result");
        answerSheet.setAttribute("action","add");
        answerSheet.setProperty("source_id",tgt_meeting);
        answerSheet.setProperty("related_id",moving.getProperty("related_id"));
        answerSheet.setProperty("in_answer",moving.getProperty("in_answer"));
        answerSheet.setProperty("in_surveytype",moving.getProperty("in_surveytype"));
    
        answerSheet.setProperty("in_participant",applicant.getID());
        Item last=answerSheet.apply();   
        
        
        
    }
        

if(message!=""){
    return inn.newResult(message);    
}else{
    return applicant;    
}



#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Move_MeetingUser' and [Method].is_current='1'">
<config_id>2561CC7A63324A8C83C5DF98D6AA270E</config_id>
<name>In_Move_MeetingUser</name>
<comments>將會議使用者從A會議移動到B會議，由內部applyMethod使用，原則上不要從外部呼叫。</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
