/*
撰寫:JOE
目的:計算欄位中的數值(合約明細)
做法:
1.使用SumRelationshipPropertyToSourceProperty
位置:onAfterAdd,onAfterUpdate
*/

//System.Diagnostics.Debugger.Break();

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
double dblSum = 0;
//合約明細
//1.使用SumRelationshipPropertyToSourceProperty
dblSum = _InnH.SumRelationshipPropertyToSourceProperty(this,"in_contract_detail","in_money_tax","in_allow_amount");

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Total_Tax' and [Method].is_current='1'">
<config_id>55F4AF1A45D942498EBA074F46BD0E29</config_id>
<name>In_Total_Tax</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
