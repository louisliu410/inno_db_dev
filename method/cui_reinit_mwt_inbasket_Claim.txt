if (inArgs.isReinit) {
	var topWindow = aras.getMostTopWindowWithAras(window);
	var workerFrame = topWindow.work;
	var claimFlg = false;
	if (workerFrame && workerFrame.grid) {
		var rowID = workerFrame.grid.getSelectedId();
		if (rowID) {
			var activityType = workerFrame.grid.getUserData(rowID, 'assignmentType');
			if (activityType === 'workflow') {
				var lockType = workerFrame.grid.getUserData(rowID, 'lockType');
				var claimFlg = lockType == '0';
			}
		}
	}

	return {'cui_disabled': !claimFlg};
}
return {};

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_reinit_mwt_inbasket_Claim' and [Method].is_current='1'">
<config_id>B8F93A032EE74341AC0B350905FBAC58</config_id>
<name>cui_reinit_mwt_inbasket_Claim</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
