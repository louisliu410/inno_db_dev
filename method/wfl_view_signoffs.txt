var ProcessItem = parent.item;
var processID;
try {
	processID = ProcessItem.getAttribute('id');
} catch (e) {}

if (!processID) {return;}

var ProcessItem = aras.getItemById('Workflow Process',processID);
if (!(ProcessItem)) {return;}
var ProcessTypeID = ProcessItem.getAttribute('type');
var report = aras.getItemByKeyedName('Report','Workflow Process History');
aras.runReport(report,ProcessTypeID,ProcessItem);

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='wfl_view_signoffs' and [Method].is_current='1'">
<config_id>BDA02A8FC5704D7884E10AD6A2556944</config_id>
<name>wfl_view_signoffs</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
