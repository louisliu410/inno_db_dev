if (this.work && this.work.grid) {
	var workFrame = this.work;
	var selectedIds = workFrame.grid.getSelectedItemIds();
	if (selectedIds.length === 0 || selectedIds.length > 1) {
		return;
	}

	var itemNode = aras.getItemById(workFrame.itemTypeName, selectedIds[0], 0);
	if (itemNode) {
		var discoverOnlyFlg = (itemNode.getAttribute('discover_only') == '1');
		var lockedBy = aras.getItemProperty(itemNode, 'locked_by_id');

		var userID = aras.getCurrentUserID();
		var viewFlg = (lockedBy != userID && !discoverOnlyFlg);
		var isViewEnabled = viewFlg && !isFunctionDisabled(workFrame.itemTypeName, 'View');

		if (isViewEnabled) {
			this.menu.processCommand('view');
		} else {
			var isTemp = aras.isTempEx(itemNode);
			var editFlg = ((isTemp || lockedBy == userID) && !discoverOnlyFlg);
			var lockFlg = aras.uiItemCanBeLockedByUser(itemNode, workFrame.isRelationshipIT, workFrame['use_src_accessIT']);
			var isEditEnabled = ((lockFlg || editFlg) && !discoverOnlyFlg) && !isFunctionDisabled(workFrame.itemTypeName, 'Edit');
			if (isEditEnabled) {
				this.menu.processCommand('edit');
			}
		}
	}
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mws_ctrl_enter' and [Method].is_current='1'">
<config_id>67BE765EC195472DBA3E3F1816CEA334</config_id>
<name>cui_default_mws_ctrl_enter</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
