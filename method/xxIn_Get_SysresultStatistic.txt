Innovator inn=this.getInnovator();

string strMeetingId=this.getProperty("meeting_id","no_data"); //目標會議id
string strSurveyType=this.getProperty("surveytype","2"); //目標問卷類型

if(strMeetingId=="no_data" || strSurveyType=="no_data"){
    return inn.newError("missing required parameter : \"meeting_id\" or \"surveytype\"");
}

Item itmPassengers=this.apply("In_Collect_PassengerParam");
Item itmRst=inn.newItem();
Item itmAllSurveys;
string strAllSurveySQL=@"select related_id from In_Meeting_Surveys where source_id='{#meeting_id}'  and in_surveytype='{#surveytype}'
                            order by sort_order"
                        .Replace("{#meeting_id}",strMeetingId)
                        .Replace("{#surveytype}",strSurveyType);

itmAllSurveys=inn.applySQL(strAllSurveySQL);

string strCountingCorrectSQL=@"select count(in_sysresult) inn_count from In_Meeting_Surveys_result where source_id='{#meeting_id}' and 
        in_surveytype='{#surveytype}' and related_id='{#surveyid}' and in_sysresult='1' "
        .Replace("{#meeting_id}",strMeetingId)
        .Replace("{#surveytype}",strSurveyType);
string strCountingFalseSQL=@"select count(in_sysresult) inn_count from In_Meeting_Surveys_result where source_id='{#meeting_id}' and 
        in_surveytype='{#surveytype}' and related_id='{#surveyid}' and in_sysresult='0' " 
        .Replace("{#meeting_id}",strMeetingId)
        .Replace("{#surveytype}",strSurveyType);
string strCountingEmptySQL=@"select count(in_sysresult) inn_count from In_Meeting_Surveys_result where source_id='{#meeting_id}' and 
        in_surveytype='{#surveytype}' and related_id='{#surveyid}' and (in_sysresult is null or in_sysresult='') " 
        .Replace("{#meeting_id}",strMeetingId)
        .Replace("{#surveytype}",strSurveyType);
string strSurveyAML=@"
        <AML>   
            <Item type='In_Survey' action='get' id='{#id}' />
        </AML>";

int intAllSurveyCount=itmAllSurveys.getItemCount();
for(int c=0;c<intAllSurveyCount;c++){
    string strSurveyId=itmAllSurveys.getItemByIndex(c).getProperty("related_id");
    string strTmp=strSurveyAML.Replace("{#id}",strSurveyId);
    string strCorrectSQL=strCountingCorrectSQL.Replace("{#surveyid}",strSurveyId);
    string strFalseSQL=strCountingFalseSQL.Replace("{#surveyid}",strSurveyId);
    string strEmptySQL=strCountingEmptySQL.Replace("{#surveyid}",strSurveyId);
    Item itmTmpSurvey=inn.applyAML(strTmp);
    Item itmCorrect=inn.applySQL(strCorrectSQL);
    Item itmFalse=inn.applySQL(strFalseSQL);
    Item itmEmpty=inn.applySQL(strEmptySQL);
    itmTmpSurvey.setProperty("inn_correct",itmCorrect.getProperty("inn_count"));
    itmTmpSurvey.setProperty("inn_false",itmFalse.getProperty("inn_count"));
    itmTmpSurvey.setProperty("inn_empty",itmEmpty.getProperty("inn_count"));
    itmRst.addRelationship(itmTmpSurvey);
    
}
int passengerCount=itmPassengers.getItemCount();
for(int p=0;p<passengerCount;p++){
   itmRst.addRelationship(itmPassengers.getItemByIndex(p)); 
}
//itmRst.dom.GetElementsByTagName("Relationships")[0].InnerXml=itmPassengers.node.InnerXml;


return itmRst;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='xxIn_Get_SysresultStatistic' and [Method].is_current='1'">
<config_id>5D1BF27FA69A4C059FE61C8C7BCEB0D4</config_id>
<name>xxIn_Get_SysresultStatistic</name>
<comments>取得問卷答對與答錯的數量，由c.aspx呼叫</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
