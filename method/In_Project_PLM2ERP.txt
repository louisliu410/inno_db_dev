/*
目的:將專案主檔拋轉到ERP
說明:
做法:
1.建立一個Queue
*/
//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

Item itmQueue = _InnH.CreateQueue(this,"plm2erp-project","<project_id>" + this.getID() + "</project_id>",""); //(Item Context, string QueueType,string Param,string StateMethodName)

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Project_PLM2ERP' and [Method].is_current='1'">
<config_id>D2C653261CB7434189FD0821139918EC</config_id>
<name>In_Project_PLM2ERP</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
