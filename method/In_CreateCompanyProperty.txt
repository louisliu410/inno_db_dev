//System.Diagnostics.Debugger.Break();

Innovator inn = this.getInnovator();

string aml = "";

aml = "<AML>";
aml += "<Item type='Property' action='get'>";
aml += "<name>in_company</name>";
aml += "<source_id>" + this.getID() + "</source_id>";
aml += "</Item></AML>";
Item itmProperty = inn.applyAML(aml);

if(itmProperty.isError())
{
    aml = "<AML>";
    aml += "<Item type='List' action='get'>";
    aml += "<name>In_Company</name>";
    aml += "</Item></AML>";
    Item itmList = inn.applyAML(aml);
    
    if(this.getProperty("name")!="ItemType"&&this.getProperty("name")!="Property")
    {
        aml = "<AML>";
	    aml += "<Item type='Property' action='add'>";
	    aml += "<source_id>" + this.getID() + "</source_id>";
	    aml += "<name>in_company</name>";
	    aml += "<label xml:lang='zt'>公司別</label>";
	    aml += "<data_type>list</data_type>";
	    aml += "<data_source>" + itmList.getID() + "</data_source>";
	    aml += "<is_hidden>1</is_hidden>";
	    aml += "<is_hidden2>1</is_hidden2>";
	    aml += "</Item></AML>";
	    Item itmPrty = inn.applyAML(aml);
    }
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_CreateCompanyProperty' and [Method].is_current='1'">
<config_id>759A5049D83C47EC95E5613C1A967320</config_id>
<name>In_CreateCompanyProperty</name>
<comments>任何新物件創建時就帶有公司別屬性</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
