/*

*/

//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";


Item itmR = this;
string strNGId = "";
string strOkId = "";
try
{
	aml = "<AML>";
	aml += "<Item type='in_proposal' action='get'>";
	aml += "<in_number condition='in'>";
	aml += "'P0000288','P0000279','P0000290'";
	aml += "</in_number>";
	aml += "</Item></AML>";
	Item itmProposals = inn.applyAML(aml);
	
	for(int i=0;i<itmProposals.getItemCount();i++)
	{
		Item itmProposal =itmProposals.getItemByIndex(i);
		try
		{
			Item itmTmp = itmProposal.apply("In_ProposalToProject_S");
			strOkId = itmProposal.getID() + ",";
			
		}
		catch(Exception ex)
		{
			strNGId += itmProposal.getID() + ",";
			string strMsg = strNGId + ":" + ex.Message;
			Innosoft.InnUtility.AddLog(strMsg,"ProposalToProject");
		}
	}
	Innosoft.InnUtility.AddLog("NG:" + strNGId,"ProposalToProject");
	Innosoft.InnUtility.AddLog("OK:" + strOkId,"ProposalToProject");
	
	
}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);


	string strMethodName = System.Reflection.MethodBase.GetCurrentMethod().ToString();
	strMethodName = strMethodName.Replace("Aras.IOM.Item methodCode(Aras.Server.Core.","");
	strMethodName = strMethodName.Replace("EventArgs)","");
	string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;


	string strError = ex.Message + "\n";
	//strError += (ex.InnerException==null?"":ex.InnerException.Message + "\n");

	if(aml!="")
    	strError += "無法執行AML:" + aml  + "\n";

	if(sql!="")
		strError += "無法執行SQL:" + sql  + "\n";
		
	string strErrorDetail="";
	strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
	//return inn.newError(_InnH.Translate(strError));
	throw new Exception(_InnH.Translate(strError));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_ProposalToProject_All' and [Method].is_current='1'">
<config_id>8A02E003A6944E9781B3C17F1F7F49C8</config_id>
<name>In_ProposalToProject_All</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
