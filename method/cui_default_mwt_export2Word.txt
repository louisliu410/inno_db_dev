var topWindow = aras.getMostTopWindowWithAras(window);
var workerFrame = topWindow.work;
var gridXmlCallback = function() {
	return workerFrame.grid.getXML(false);
};
aras.export2Office(gridXmlCallback, 'export2Word');

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwt_export2Word' and [Method].is_current='1'">
<config_id>B5EE8019EBA140AAA1C4ECDD78D75415</config_id>
<name>cui_default_mwt_export2Word</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
