if (inArgs.isReinit) {
	//Workflow Map, Form, Life Cycle Map, CMF Admin Panel Toolbar
	var node;
	if (window.itemTypeName === 'Form' || window.itemTypeName === 'cmf_ContentType' || window.itemTypeName === 'Life Cycle Map') {
		node = window.item;
	} else {
		node = window.currWFNode;
	}

	if (node) {
		var isRootAdmin = aras.getLoginName().search(/^admin$|^root$/) === 0;
		var isLocked = aras.isLocked(node);
		var isTemp = aras.isTempEx(node);
		return {'cui_disabled': !((isEditMode || isRootAdmin  && isLocked) && !isTemp)};
	}
	//Normal Toolbar
	if (Object.getOwnPropertyNames(inArgs.eventState).length === 0) {
		var states = aras.evalMethod('cui_reinit_calc_tearoff_states');
		var keys = Object.keys(states);
		for (var i = 0; i < keys.length; i++) {
			inArgs.eventState[keys[i]] = states[keys[i]];
		}
	}
	return {'cui_disabled': !inArgs.eventState.isUnlock};
}
return {};

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_reinit_twt_unlock' and [Method].is_current='1'">
<config_id>6EB13A3F8DE745C4BE715EADEEAA4C14</config_id>
<name>cui_reinit_twt_unlock</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
