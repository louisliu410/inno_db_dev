
Innovator inn=this.getInnovator();

string strMeetingId=this.getProperty("meeting_id","no_data");
string strSurveyType=this.getProperty("surveytype","2");
string strMode=this.getProperty("data_mode","no_data");//要取得主頁或是內容
string strUserId=this.getProperty("muid","nod_data");
Item itmPassengers=this.apply("In_Collect_PassengerParam");

Item itmRst;

switch(strMode){
	
	case "mainpage":
		if(strMeetingId=="no_data"){return inn.newError("missing required parameter \"meeting_id\" or \"data_mode\"");}
		string strMeetingRelationAML=@"
			<AML>
    			<Item type=""In_Meeting"" action=""get"" id=""{#meeting_id}"" select=""id,in_title,keyed_name,managed_by_id,owned_by_id,in_address"">
    			    <Relationships>
    			        <Item type=""In_Meeting_User"" action=""get"">
    					<in_note_state>official</in_note_state>
    				</Item>
    			    </Relationships>
    			</Item>
			</AML>
		".Replace("{#meeting_id}",strMeetingId);
		itmRst=inn.applyAML(strMeetingRelationAML);
	break;
	case "data":
	    Item itmSurveyResult;
		if(strUserId=="no_data"){return inn.newError("missing required parameter \"muid\"");}
		string strSurveyResultAML=@"
			<AML>
				<Item type=""In_Meeting_Surveys_result"" action=""get"">
					<in_participant>{#muid}</in_participant>
					<in_surveytype>{#surveytype}</in_surveytype>
				</Item>
			</AML>
		"
		.Replace("{#muid}",strUserId)
		.Replace("{#surveytype}",strSurveyType);
		itmSurveyResult=inn.applyAML(strSurveyResultAML);
		
		if(itmSurveyResult.isError() || itmSurveyResult.isEmpty()){
		    string strErrorReturnAML=@"
				<Item>
					<Relationships>
						<Item type=""In_Meeting_Surveys_result"">
						<inn_ans_status>no_answer</inn_ans_status>
						<answer>no_answer</answer>
						</Item>
					</Relationships>
				</Item>	";
				itmRst=inn.newItem();
				itmRst.loadAML(strErrorReturnAML);
		}else{
		    itmRst=inn.newItem();
			int musCount=itmSurveyResult.getItemCount();
			for(int j=0;j<musCount;j++){
			    Item tmp=itmSurveyResult.getItemByIndex(j);
				tmp.setProperty("inn_ans_status","answered");
				itmRst.addRelationship(tmp);
			}
		}
	break;
	default:itmRst=inn.newError("multiple    required missing or incorrect  paramter_names: \"meeting_id\",\"surveytype\",\"data_mode\" or \"muid\"");break;
	
}

if(!itmRst.isError()){
    int paramCount=itmPassengers.getItemCount();
    for(int pc=0;pc<paramCount;pc++){
        itmRst.addRelationship(itmPassengers.getItemByIndex(pc));
    }
}


return itmRst;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Get_SurveyResultOrUser' and [Method].is_current='1'">
<config_id>72866BB2B66E4CD8AB02B3EB8370BCDD</config_id>
<name>In_Get_SurveyResultOrUser</name>
<comments>取得In_Meeting_UserScoreView.html所需要的資料，由c.aspx呼叫。</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
