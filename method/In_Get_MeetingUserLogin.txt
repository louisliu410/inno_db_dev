//若有傳入指定會議id: meeting_id，會傳回inn_is_active，指定的會議是否仍在報名期間內。若無傳則無條件傳回false。


Innovator inn=this.getInnovator();

Item meeting;
Item result=inn.newItem();
Item meetingFunctionTimes;
string meeting_id=this.getProperty("meeting_id","no_data");
string isFunction="";
string getFunctionTimeAML=@"<AML>
								<Item type=""In_Meeting_Functiontime"" action=""get"" select=""id"">
									<source_id>{#meeting_id}</source_id>
									<in_action>mulogin</in_action>
									<in_date_s condition=""lt"">{#today}</in_date_s>
									<in_date_e condition=""gt"">{#today}</in_date_e >
								</Item>
							</AML>"
							.Replace("{#meeting_id}",meeting_id)
							.Replace("{#today}",System.DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));

if(meeting_id=="no_data"){
    isFunction="";
}else{
	meetingFunctionTimes=inn.applyAML(getFunctionTimeAML);
	if(meetingFunctionTimes.isError()||meetingFunctionTimes.getItemCount()<=0){
	result.setProperty("inn_is_active","false");
	isFunction="false";
	}else{
		result.setProperty("inn_meeting_id",meeting_id);
		isFunction="true";
	}
}

meeting=inn.newItem("In_Meeting","get");
meeting.setAttribute("select","id,keyed_name,in_title");
meeting=meeting.apply();
int mtCount=meeting.getItemCount();
for(int count=0;count<mtCount;count++){
	result.addRelationship(meeting.getItemByIndex(count));
}
result.setProperty("inn_is_active",isFunction);

return result;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Get_MeetingUserLogin' and [Method].is_current='1'">
<config_id>23295C6D5CA24D4EBD87AE1F8CCC325D</config_id>
<name>In_Get_MeetingUserLogin</name>
<comments>取得MeetingUserLogin所需的資料，由b.aspx使用。</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
