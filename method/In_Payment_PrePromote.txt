/*
目的:PrePromote
*/

//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";
string err = "";
	
try
{
	string strFromState = this.getPropertyItem("transition").getPropertyAttribute("from_state","keyed_name");
	string strToState = this.getPropertyItem("transition").getPropertyAttribute("to_state","keyed_name");
	string strFromTo = strFromState + "_to_" + strToState;
	
	switch(strToState)
	{
		case "Released":
           
			break;
		default:
			break;
		
	}
	
	if(err!="")
		throw new Exception(err);


		
}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
	throw new Exception(ex.InnerException==null?ex.Message:ex.InnerException.Message);
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return this;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Payment_PrePromote' and [Method].is_current='1'">
<config_id>7AFAE8A01716488BBDB5667180BF568F</config_id>
<name>In_Payment_PrePromote</name>
<comments>三張單據的 prePromote</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
