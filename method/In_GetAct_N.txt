/*
目的:取得單一Activity
參數: 
1.傳入Activity ID(actid)
2.ActivityAssignment ID(actassid)
3.Itemtype, itemid
做法:
A.取得Activity
1.傳入ActivityAssignment ID (簽核relationship)
-->取得ActivityID
2.傳入Itemtype, itemid
-->取得目前執行中的ActivityID
3.傳入Activity ID
4.判斷
-->該Activity狀態應為Active
-->目前的使用者是否為該Activity的一員
-->該ActivityAssignment是否已簽過了

B.提供資料
1.取得workflowMAP
2.取得activity Task,Path,variable
//Path只挑選 關聯任務中不含cloned_as property 或 有 cloned_as property,但 attribute為 is_null
3.取得ControlledItem
4.取得Signoff

*/
string strMethodName = "In_GetAct_N";
//System.Diagnostics.Debugger.Break();
/* Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);
 */
Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";

_InnH.AddLog(strMethodName,"MethodSteps");
Item itmR = this;

try
{
	string strActAssId = this.getProperty("actassid","");
	string strActId = this.getProperty("actid","");
	string strItemtype = this.getProperty("itemtype","");
	string strItemId = this.getProperty("itemid","");
	string strLang = this.getProperty("lang","zt");
	string strNewId = this.getProperty("newid","");
	string identity_list = Aras.Server.Security.Permissions.Current.IdentitiesList; 
	string strGoBack = this.getProperty("goback","");
	Item itmWFP;
	Item itmControlledItem=null;
	if(strActId!="")
	{
		sql = "select id from [Activity_Assignment] where source_id='" + strActId + "' and related_id in(";
		sql += "'" + identity_list.Replace(",","','") + "'";
		sql += ")";
		Item itmTemp = inn.applySQL(sql);

		if(itmTemp.getResult()!="")
			strActAssId = itmTemp.getID();
		else
			throw new Exception("無簽審權限");

	}

	if(strItemtype!="" && strItemId!="")
	{		
		itmControlledItem = inn.getItemById(strItemtype,strItemId);
		string strActivityId = itmControlledItem.getProperty("in_current_activity_id","");

		aml = "<AML>";
		aml += "<Item type='Activity Assignment' action='get' select='id'>";
		aml += "<source_id>" + strActivityId + "</source_id>";
		aml += "<related_id condition='in'>" + identity_list + "</related_id>";
		aml += "</Item>";
		aml += "</AML>";

		Item actass = inn.applyAML(aml);
		if(actass.isError())
		{
			throw new Exception("無簽審權限");
		}
		strActAssId = actass.getItemByIndex(0).getID();
	}


	aml = "<AML>";
	aml += "<Item type='Activity' action='get'>";
	aml +="	<Relationships>";
	aml +="		<Item type='Activity Assignment' action='get'>";
	aml +="					<id>" + strActAssId + "</id>";
	aml +="				</Item>";
	aml +="		<Item type='Activity Task' action='get'/>";
	aml +="		<Item type='Activity Variable' action='get'/>";
	aml +="		<Item type='Workflow Process Path' action='get' select='id,name,label,related_id(cloned_as)'/>";
	aml +="	</Relationships>";
	aml +="</Item>";
	aml += "</AML>";

	Item Act = inn.applyAML(aml);
	string ActivityID="";
	if(Act.isError())
	{
		throw new Exception(Act.getErrorString());
	}
	else
	{
		ActivityID = Act.getID();
	}
	
	//Path只挑選 關聯任務中不含cloned_as property 或 有 cloned_as property,但 attribute為 is_null
	Item itmPaths = Act.getRelationships("Workflow Process Path");
	List<Item> arrRemoveItems= new List<Item>(); 
	for(int i=0;i<itmPaths.getItemCount();i++)
	{
		Item itmPath =  itmPaths.getItemByIndex(i);
		if(itmPath.getProperty("label","")=="")
			itmPath.setProperty("label",itmPath.getProperty("name"));
		if(itmPath.getPropertyItem("related_id").getProperty("cloned_as","null")=="null")
			continue;
		
		if(itmPath.getPropertyItem("related_id").getPropertyAttribute("cloned_as","is_null","")=="1")
			continue;
		
		arrRemoveItems.Add(itmPath);
	}
	foreach (Item itmPath in arrRemoveItems)
	{
		itmPaths.removeItem(itmPath);
	}

	//取得workflow_name,attachedType(Itemetype),attachedId(Item id),workflowProcessId
	//Item-(workflow)-Workflow Process-(workflow Process Activity)-Activity

	 itmWFP = _InnH.GetInnoWorkflowProcessByActivity(Act);
	string workflowProcessId = itmWFP.getID();
	string workflow_name = itmWFP.getProperty("name","");

	//string attachedType = itmWFP.getProperty("in_itemtype","");
	//string attachedId = itmWFP.getProperty("in_item_id","");
    string in_flowchart = itmWFP.getProperty("in_flowchart","");
	
	
	Act.setProperty("workflow_name",workflow_name);
	//Act.setProperty("workflowProcessId",workflowProcessId);
	//Act.setProperty("attachedType",attachedType);
	//Act.setProperty("attachedId",attachedId);
	Act.setProperty("in_flowchart",in_flowchart);


	

	//補上 comments
	string strComments =Act.getRelationships("Activity Assignment").getItemByIndex(0).getProperty("comments","");
	Act.setProperty("comments",strComments);

	//補上 AssignmentID
	Act.setProperty("actassid",strActAssId);

	//補上 task_ids
	Item itmTasks = Act.getRelationships("Activity Task");
	string strTaskIds = "";
	for(int i=0;i<itmTasks.getItemCount();i++)
	{
		Item itmTask  = itmTasks.getItemByIndex(i);
		strTaskIds += itmTask.getID() + ",";
	}
	Act.setProperty("task_ids",strTaskIds.Trim(','));

//    Item SignOff = inn.applyMethod("In_GetSignOff_N","<workflow_process_id>" + workflowProcessId + "</workflow_process_id>");
//
//	Item itmSignOffs = SignOff.getRelationships("signoff");
//	for(int i=0;i<itmSignOffs.getItemCount();i++)
//	{
//		Item itmSignOff = itmSignOffs.getItemByIndex(i);
//		Act.addRelationship(itmSignOff);
//	}
//	
	
	if(strNewId!="")
	    Act.setID(strNewId);
	itmR = Act;
	
	//簽審後是否要返回前頁
	itmR.setProperty("goback",strGoBack);
	
	//是否要秀出挑選檔案
	Item itmRel = inn.newItem("RelationshipType","get");
	strItemtype = Act.getProperty("in_itemtype","");
	//strItemtype = strItemtype.Replace(" ","_");
	itmRel.setProperty("name", strItemtype + "_File");
	itmRel = itmRel.apply();
	if(!itmRel.isError())
		itmR.setProperty("show_choosefile","true");




}
catch(Exception ex)
{	
	//if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);


	string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;


	string strError = ex.Message + "\n";

	if(aml!="")
    	strError += "無法執行AML:" + aml  + "\n";

	if(sql!="")
		strError += "無法執行SQL:" + sql  + "\n";

	string strErrorDetail="";
	strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	//_InnH.AddLog(strErrorDetail,"Error");
	strError = strError.Replace("\n","</br>");
	throw new Exception(_InnH.Translate(strError));
	
}
//if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_GetAct_N' and [Method].is_current='1'">
<config_id>6492ACB2287C426C81AFC00E806B6937</config_id>
<name>In_GetAct_N</name>
<comments>取得單一Activity</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
