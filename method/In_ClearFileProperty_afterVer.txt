/*
目的:
1.如果原始檔案被置換,則過程word檔案與PDF都要被清除
2.如果過程word檔被修改,則PDF檔案要被清除
位置:onbeforeupdate
做法:
1.如果in_native_file 不是 null, 代表本次有修改,則將in_word_file與in_modify_file都清空
2.如果in_word_file 不是 null, 代表本次有修改,則將in_modify_file都清空
*/
//In_ClearFileProperty_after

//System.Diagnostics.Debugger.Break();
RequestState.Add("in_native_file",this.getProperty("in_native_file",""));
RequestState.Add("in_word_file",this.getProperty("in_word_file",""));
return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_ClearFileProperty_afterVer' and [Method].is_current='1'">
<config_id>595F02E0DECC4425BDCFBB35BB81B52C</config_id>
<name>In_ClearFileProperty_afterVer</name>
<comments>inn drug</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
