' name: SQL onAfterUpdate
' created: 5-OCT-2005 George J. Carrette
'MethodTemplateName=VBScriptMain;

Sub Main(inDom As XmlDocument,outDom As XmlDocument,Iob As Object,Svr As Object,Ses As Object)
 cco.xml.setitemproperty(cco.xml.GetRequestItem(indom),"PROCESS","onBeforeDelete")
 cco.applyitem.ExecuteMethodByName(indom,outdom,"SQL PROCESS",True)
End Sub

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='SQL onBeforeDelete' and [Method].is_current='1'">
<config_id>1D0406B6BC1B4F799EBA38E4509B5325</config_id>
<name>SQL onBeforeDelete</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
