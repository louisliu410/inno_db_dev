Innovator inn = this.getInnovator();

try
{
	inn.ConsumeLicense("Aras.EnterpriseSearch");
}
catch (Exception exception)
{
    return inn.newError(exception.Message);
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='ES_CheckFeature' and [Method].is_current='1'">
<config_id>8B2BB39511304DFEB4AA53B9FABB399F</config_id>
<name>ES_CheckFeature</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
