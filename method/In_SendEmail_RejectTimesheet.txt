/*
目的:工時審核
作法:
1.取得工時審核(只取 In Review的工時卡處理)
2.將退回並附有說明的工時紀錄表填入說明與審核狀態並將狀態推為Rejected
3.將退回並無說明的工時紀錄表加入提醒
4.將核准的工時紀錄表填入審核狀態並將狀態推為Approved
5.刪除符合審核條件的關聯
位置:
專案工時審核表(物件動作)
*/

//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

int intAll = 0;
int intReject = 0;
int intApproved = 0;

Item itmSQL = null;
Item itmTimesheet = null;

string aml = "";
string sql = "";

string strDelID = "";
string strErrMsg = "";

string strItemType = this.getProperty("ItemType","");
string strSourceID = this.getProperty("SourceID","");

//1.取得工時審核
aml = "<AML>";
aml += "<Item type='" + strItemType + "' action='get'>";
aml += "<source_id>" + strSourceID + "</source_id>";
aml += "</Item></AML>";
Item itmTimesheets = inn.applyAML(aml);

for(int i=0;i<itmTimesheets.getItemCount();i++){
    itmTimesheet = itmTimesheets.getItemByIndex(i);
	Item itmTimeRecord = inn.getItemById("In_TimeRecord",itmTimesheet.getProperty("related_id"));
	
	//只取 In Review的工時卡處理
	if(itmTimeRecord.getProperty("state","")!="In Review")
	{
		continue;
	}
	
		
	
	intAll ++;
	
	//2.將退回並附有說明的工時紀錄表填入說明與審核狀態並將狀態推為Rejected	
	if(itmTimesheet.getProperty("in_verify","") != "1"){
	    if(itmTimesheet.getProperty("in_meno","") != ""){
	    _InnH.SendEmail(itmTimesheet.getID(),"In_WorkTime_Reject",itmTimesheet.getProperty("in_employee"));
	    
	    intReject ++;
	    
	    strDelID += itmTimesheet.getProperty("id")+",";
	    
	    sql = "Update In_TimeRecord set ";
	    sql += "in_reject_note = '" + itmTimesheet.getProperty("in_meno","") + "'";
	    //sql += ",in_approved_state = 'Reject'";
	    sql += " where id = '" + itmTimeRecord.getID() + "'";
	    inn.applySQL(sql);
	    
	    itmTimeRecord.setProperty("state","Rejected");
	    itmTimeRecord = itmTimeRecord.apply("promoteItem");
	    
	    //3.將退回並無說明的工時紀錄表加入提醒
	    }else{
	        strErrMsg += "#項次" + itmTimesheet.getProperty("sort_order","") + ":" + "核退前請填入退回說明\n";
	    }
	}
	
	//4.將核准的工時紀錄表填入審核狀態並將狀態推為Approved
	if(itmTimesheet.getProperty("in_verify","") == "1"){
	    if(itmTimesheet.getProperty("in_meno","") == ""){
	        
	        intApproved ++;
	        
	        strDelID += itmTimesheet.getProperty("id")+",";
	        
	        sql = "Update In_TimeRecord set ";
	        sql += "in_approved_state = 'Approved'";
	        sql += " where id = '" + itmTimeRecord.getID() + "'";
	        //itmSQL = inn.applySQL(sql);
	        
	        itmTimeRecord.setProperty("state","Approved");
	        itmTimeRecord = itmTimeRecord.apply("promoteItem");
	    }else{
	        strErrMsg += "#項次" + itmTimesheet.getProperty("sort_order","") + ":" + "核准後請勿填入退回說明\n";
	    }
	}
}
strDelID = strDelID.Trim(',');

//5.刪除符合審核條件的關聯
aml = "<AML>";
aml += "<Item type='" + strItemType + "' action='delete' where=\"[id]='" + strDelID + "'\">";
aml += "</Item></AML>";
Item itmDel = inn.applyAML(aml);

Item itm_R = inn.newItem("r");
itm_R.setProperty("ErrMsg",strErrMsg);
itm_R.setProperty("all",intAll.ToString());
itm_R.setProperty("reject",intReject.ToString());
itm_R.setProperty("approved",intApproved.ToString());

if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itm_R;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_SendEmail_RejectTimesheet' and [Method].is_current='1'">
<config_id>875028AEE77741D2BA3F3A1FCE34C3E8</config_id>
<name>In_SendEmail_RejectTimesheet</name>
<comments>工時審核</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
