//用於藥證變更程序:當新版藥證Release之後,必須把舊版藥證從In Change推到Superseded
//in_DA_PromoteOld
Innovator inn = this.getInnovator();
if(this.getProperty("in_old_licno","")=="")
	return this;
Item OldItem = inn.getItemById(this.getType(),this.getProperty("in_old_licno",""));

  
  OldItem.setProperty("state", "Superseded");
  OldItem = OldItem.apply("promoteItem");
  
  if(OldItem.isError())
  {
	throw new Exception(OldItem.getErrorString());
  }
  
  return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_DA_PromoteOld' and [Method].is_current='1'">
<config_id>3FA237F1FEC2459EB18AA3196C123E32</config_id>
<name>In_DA_PromoteOld</name>
<comments>inn</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
