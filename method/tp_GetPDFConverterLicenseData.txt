string licenseId = this.getID();
Aras.Server.Security.Identity adminIdentity = null;
bool permsWasSet = false;
try
{
	adminIdentity = Aras.Server.Security.Identity.GetByName("Administrators");
	permsWasSet = Aras.Server.Security.Permissions.GrantIdentity(adminIdentity);
	Item queryItem = this.newItem("Feature License", "get");
	queryItem.setAttribute("select", "additional_license_data");
	queryItem.setProperty("feature", "Aras.HTMLtoPDFConverter");
	queryItem.setProperty("id", licenseId);
	Item result = queryItem.apply();
	return result;
}
finally
{
	if (permsWasSet)
	{
		Aras.Server.Security.Permissions.RevokeIdentity(adminIdentity);
	}
}
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='tp_GetPDFConverterLicenseData' and [Method].is_current='1'">
<config_id>F5A3B88A047C4E19958E114B0C9371BB</config_id>
<name>tp_GetPDFConverterLicenseData</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
