if (!inArgs || !inArgs.results || !inArgs.results.getItemByIndex(0))
{
  return;
}

var formNd = top.aras.getItemByName("Form", "PE_AddToChange", 0);

if (formNd)
{
  var param = new Object();
  param.title = "Choose Change Item";
  param.formId = formNd.getAttribute("id");
  param.aras = top.aras;
  param.itemTypeName = inArgs.results.getItemByIndex(0).getType();
  param.item = inArgs.results.getItemByIndex(0);
  param.isEditMode = true;

  var width = top.aras.getItemProperty(formNd, "width");
  var height = top.aras.getItemProperty(formNd, "height");

  var options = {
      dialogWidth: width,
      dialogHeight: height
    };
	var callback = {
		oncancel: function (dialogWrapper) {
			var result = dialogWrapper.result;
			if(!result)
			{
				return;
			}
			  
			switch(result.action)
			{
			case "create":
				//results.type
				inArgs.results.getItemByIndex(0).setAttribute("ChangeItem",result.type);
				top.aras.evalMethod("PE_AddChangeItem",inArgs.results.dom.xml);
				break;
			case "open":
				inArgs.results.getItemByIndex(0).setAttribute("ChangeItem",result.type);
				inArgs.results.getItemByIndex(0).setAttribute("ChangeItemName",result.name);
				top.aras.evalMethod("PE_AddChangeItem",inArgs.results.dom.xml);
				break;
			case "search":
				var param = { aras: top.aras, itemtypeName: result.type, multiselect: false };
				var searchedItem;
			
				var options = { dialogHeight:450, dialogWidth:700, resizable:true};
			
				param.callback =  function(dlgRes){
					if(!dlgRes)
					{
						return;
					}
				
					searchedItem = dlgRes.item;
					if(!searchedItem)
					{
						return;
					}
					
					inArgs.results.getItemByIndex(0).setAttribute("ChangeItem",result.type);
					var keyed_name = top.aras.getKeyedNameEx(searchedItem);
					inArgs.results.getItemByIndex(0).setAttribute("ChangeItemName",keyed_name);
					
					top.aras.evalMethod("PE_AddChangeItem",inArgs.results.dom.xml);
				};
				var wnd = top.aras.getMainWindow();
				wnd = wnd === top ? wnd.main : top;
				top.aras.modalDialogHelper.show('SearchDialog', wnd, param);
				break;   
			}
		}
	  };
	var wnd = top.aras.getMainWindow();
	wnd = wnd === top ? wnd.main : top;
    top.aras.modalDialogHelper.show('DefaultPopup', wnd, param, options, 'ShowFormAsADialog.html', callback);
}
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='PE_ChooseCMItem' and [Method].is_current='1'">
<config_id>32ED42A6F53948418BB4D732ABE115DB</config_id>
<name>PE_ChooseCMItem</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
