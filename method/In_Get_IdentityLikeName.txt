
//System.Diagnostics.Debugger.Break();
Innovator inn=this.getInnovator();
string name=this.getProperty("queryname","no_data");
Item user=inn.getItemById("User",inn.getUserID());

Item rst=inn.newItem();

if(name=="no_data" || name==""){
    Item nRst=inn.newItem("Identity","");
    nRst.setProperty("keyed_name","您未傳入任何參數");
    rst.addRelationship(nRst);
    return rst;}

Item query=inn.newItem("Identity","get");
query.setProperty("in_company",user.getProperty("in_company",""));
query.setProperty("is_alias","1");
Item orCondition=query.newOR();
orCondition.setProperty("name","*"+name+"*");
orCondition.setPropertyCondition("name","like");
orCondition.setProperty("keyed_name","*"+name+"*");
orCondition.setPropertyCondition("keyed_name","like");
orCondition.setProperty("in_number","*"+name+"*");
orCondition.setPropertyCondition("in_number","like");
Item leader=inn.newItem("Identity","get");
Item leaderOr=leader.newOR();
leaderOr.setProperty("keyed_name","*"+name+"*");
leaderOr.setPropertyCondition("keyed_name","like");
orCondition.setPropertyItem("in_leaderrole",leader);


query=query.apply();

int qCount=query.getItemCount();

if(qCount!=0){
    for(int x=0;x<qCount;x++){
        Item tmp=query.getItemByIndex(x);
        string deptName=tmp.getPropertyAttribute("in_dept","keyed_name","");
        string leaderRole=tmp.getPropertyAttribute("in_leaderrole","keyed_name","");
        tmp.setProperty("in_dept",tmp.getPropertyAttribute("in_dept","keyed_name",""));
        tmp.setProperty("in_leaderrole",leaderRole);
        rst.addRelationship(tmp);
    }
}else{
        Item dummyRst=inn.newItem("Identity","");
        dummyRst.setProperty("keyed_name","未取得任何符合條件之結果");
        rst.addRelationship(dummyRst);    
    }

return rst;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Get_IdentityLikeName' and [Method].is_current='1'">
<config_id>99EC63C2BE2744D79AC37C6E55E2D74C</config_id>
<name>In_Get_IdentityLikeName</name>
<comments>用姓名搜尋使用者。提供SearchResultCart.html需要的資料。由c.aspx呼叫</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
