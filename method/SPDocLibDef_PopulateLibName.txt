var spDoclibNameList = document.forms.MainDataForm.elements['sp_doclib_name'];

var spDoclibId = top.aras.getItemProperty(document.item, 'sp_doclib_id');
if (spDoclibId.length > 0) {
	var spDoclibName = top.aras.getItemProperty(document.item, 'sp_doclib_name');
	var option = new Option(spDoclibName, spDoclibId);
	if (spDoclibNameList.length > 0 && spDoclibNameList.options[0].value !== '') {
		for (var i = 0; i < spDoclibNameList.length; i++) {
			if (spDoclibNameList.options[i].value == option.value) {
				spDoclibNameList.selectedIndex = i;
				break;
			}
		}
	} else {
		spDoclibNameList.options[0] = option;
		spDoclibNameList.selectedIndex = 0;
	}
} else {
	spDoclibNameList.disabled = true;
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='SPDocLibDef_PopulateLibName' and [Method].is_current='1'">
<config_id>ABC0EEF3B9B94871BA5FB1DD7AC6DBDE</config_id>
<name>SPDocLibDef_PopulateLibName</name>
<comments>Populates document library name when form is populated</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
