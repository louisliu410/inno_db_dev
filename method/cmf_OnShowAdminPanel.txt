return ModulesManager.using(
	['aras.innovator.core.ItemWindow/ContentTypeItemWindowView',
	'aras.innovator.core.ItemWindow/DefaultItemWindowCreator']).then(function(View, Creator) {
		var view = new View(inDom, inArgs);
		var creator = new Creator(view);
		return creator.ShowView();
	});

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cmf_OnShowAdminPanel' and [Method].is_current='1'">
<config_id>A189B8E3C3A8472AA249AB18BEC5AABC</config_id>
<name>cmf_OnShowAdminPanel</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
