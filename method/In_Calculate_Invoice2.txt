/*
目的:計算預收款優惠價的稅額與合計後金額
做法:
*/

Innovator inn = this.getInnovator();

//若不是預收款,則離開
if(this.getProperty("in_invoice_class","0")!="2")
	return this;

//稅率
decimal dblTaxRate = Convert.ToDecimal(this.getProperty("in_tax_rate","0"));

//小數位數
int intDec = Convert.ToInt32(this.getProperty("in_currency_decimal","0"));

//優惠價
decimal dblFinPrice = Convert.ToDecimal(this.getProperty("in_fin_price","0"));

//優惠價稅額
decimal dblHeaderTax = Innosoft.InnUtility.Round(dblFinPrice * dblTaxRate, intDec);
decimal dblHeadFinPriceTax = dblFinPrice + dblHeaderTax;

string sql = "Update [In_Invoice] set ";
sql += "in_tax=" + dblHeaderTax;
sql += ",in_fin_price_tax =" + dblHeadFinPriceTax;
sql += " where id='" + this.getID() + "'";
Item itmTemp = inn.applySQL(sql);

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Calculate_Invoice2' and [Method].is_current='1'">
<config_id>022853E6A3C242B0926982F08C2D7231</config_id>
<name>In_Calculate_Invoice2</name>
<comments>inn flow</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
