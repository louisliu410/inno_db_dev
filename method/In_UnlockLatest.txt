
/*
    更新物件後解鎖指定物件，如果物件有版本機制，則自動解鎖最新版本。
    主要由InnoJSON呼叫。
    參數：
        itemtype: 物件的類型
        itemid:物件的id

*/

Innovator inn=this.getInnovator();
Innosoft.app _InnoApp = new Innosoft.app(inn);
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
Item itmQ=null;

string strItemtype=this.getProperty("itemtype","no_data");
string strItemid=this.getProperty("itemid","no_data");
    try{
        itmQ=_InnH.GetCurrentItemById( strItemtype,  strItemid);
       
            try{
                itmQ.unlockItem();    
            }catch(Exception ex){
                string strErrorMessage=ex.Message;
                if(strErrorMessage.Contains("ItemIsNotLocked")){
                    return inn.newResult("true");
                }else{
    	            throw new Exception(_InnH.Translate(strErrorMessage));
                }
            }
                return inn.newResult(true.ToString());
    }catch(Exception exx){
        
    string strError = exx.Message + "\n";
	string strErrorDetail="";
	string strParameter="\n itemtype="+strItemtype+" , itemid= "+strItemid;
	_InnH.AddLog(strErrorDetail+strParameter,"Error");
	strError = strError.Replace("<br/>","");
	throw new Exception(_InnH.Translate(strError+strParameter));        
    }

    
    
    

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_UnlockLatest' and [Method].is_current='1'">
<config_id>CC70C95BC081412FBDA0E260B81023A4</config_id>
<name>In_UnlockLatest</name>
<comments>更新物件後解鎖指定物件，如果物件有版本機制，則自動解鎖最新版本。</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
