// Loop through the "input" elements
var elements = document.getElementsByTagName("input");
for (var i=0; i<elements.length; i++){
 // The property name to use is the input name of the field
 var propName = elements[i].name;
 if (propName == "major_rev" ||propName == "state" || propName == "cost") {
  var propVal = document.thisItem.getProperty(propName);
  // If the property has no value, show a blank field
  if (propVal === undefined) {
   elements[i].value = "";
  } else {
   elements[i].value = propVal;
  }
 }
}
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='Fill Form Fields' and [Method].is_current='1'">
<config_id>6DE9F15831624BDEBAA13F2A827994BC</config_id>
<name>Fill Form Fields</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
