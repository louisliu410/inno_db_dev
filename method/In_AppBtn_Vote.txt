/*
目的:APPAction:簽審按鈕
用法:item.apply
須回傳:
app action
(若回傳 null 則代表不顯示按鈕)

位置:App Action的method
*/
string strMethodName = "In_AppBtn_Vote";

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";
string strError = "";

_InnH.AddLog(strMethodName,"MethodSteps");
Item itmAction = null;
if(this.fetchLockStatus()==0)
{
	//可簽審
    if(this.getProperty("in_wfp_state","")=="Active")
    {
    	string identityList = Aras.Server.Security.Permissions.Current.IdentitiesList;
    	sql = "select id,assigned_to from InBasket_Task where ";
    	sql += "itemtype='321BD822949149C597FD596B1212B85C'";
    	sql += " and status='Active'";
    	sql += " and my_assignment='1'";
    	sql += " and assigned_to in (" + identityList + ")";
    	sql += " and container='" + this.getProperty("in_wfp_id","") + "'";
    	sql += " and language_code_filter='zt'";
    	Item itmMyTask = inn.applySQL(sql);
    	if(itmMyTask.getResult()!="")
    	{
    		if(itmMyTask.getItemCount()>1)
    		{
    			string strIdentityId = inn.getUserAliases();
    			if(itmMyTask.getItemsByXPath("//Item[assigned_to='" + strIdentityId + "']")==null)
    				itmMyTask = itmMyTask.getItemByIndex(0);
    			else
    				itmMyTask = itmMyTask.getItemsByXPath("//Item[assigned_to='" + strIdentityId + "']").getItemByIndex(0);
    		}
    		if(itmMyTask.getProperty("id","") !="")
    		{
    			itmAction = inn.newItem();
    			itmAction.setType("action");
    			itmAction.setProperty("href","");
    			itmAction.setProperty("btn_type","report");
    			string strClick = "ih.ShowItemModal('" + this.getType() + "', '" + this.getID() + "', 'VoteModal.html', 'In_GetAct_N', {'newid':'" + this.getID() + "','actssid':'" + itmMyTask.getProperty("id","") + "'})";
    			itmAction.setProperty("click",strClick);
    			itmAction.setProperty("label","簽審");
                itmAction.setProperty("class","");
                itmAction.setProperty("data_toggle","");
                itmAction.setProperty("data_target","");
    				
    		}
    	}
    }    
}


return itmAction;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_AppBtn_Vote' and [Method].is_current='1'">
<config_id>D91B23D72B054F9084DFF006C853DE48</config_id>
<name>In_AppBtn_Vote</name>
<comments>APPAction:簽審按鈕</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
