/*
目的:PrePromote
*/

//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";
string err = "";
	
try
{
	string strFromState = this.getPropertyItem("transition").getPropertyAttribute("from_state","keyed_name");
	string strToState = this.getPropertyItem("transition").getPropertyAttribute("to_state","keyed_name");
	string strFromTo = strFromState + "_to_" + strToState;
	
	switch(strToState)
	{
		case "Released":
            this.apply("In_UpdateContractMomey");
            switch(this.getType())
            {
                case "In_Invoice":
                    if(this.getProperty("in_debit_status","")=="4"||this.getProperty("in_debit_status","")=="5") 
                    {//客戶請款的"事後扣款" 不應該拋出給ERP
                    }
                    else
                        this.apply("In_PLM2ERP");
                    break;
                default:
					this.apply("In_PLM2ERP");
                    break;
            }
			break;
		case "Obsolete":
            //作廢的時候順便把表身與表頭金額都歸零
            string strRelName = "";
            string strInPros= "";
            string strRelPros= "";
            string strMethod = "";
            switch(this.getType())
            {
                case "In_Invoice":
                    strRelName="In_Invoice_detail_2";
                    strRelPros = "in_act_debit_o=0";
                    strRelPros += ",in_debit_o=0";
                    strRelPros += ",in_invoice_tax_o=0";
                    strRelPros += ",in_nonperiodic_o=0";
                    strRelPros += ",in_payment_o=0";
                    strRelPros += ",in_retention_o=0";

                    strInPros = "in_debit_o=0";
                    strInPros += ",in_payment_o=0";
                    strInPros += ",in_invoice_tax_o=0";
                    strInPros += ",in_invoice_o=0";
                    strInPros += ",in_tax_o=0";
                    strInPros += ",in_act_collection_o=0";
                    strInPros += ",in_retention_o=0";
                    
                    
                    
                    
                    break;
                case "In_Vendor_Payment":
                    strRelName="In_Vendor_Payment_detail_2";
                    strRelPros = "in_act_collection_o=0";
                    strRelPros += ",in_debit_o=0";
                    strRelPros += ",in_payment_o=0";
                    strRelPros += ",in_retention_o=0";
                    
                    strInPros = "in_act_collection_o=0";
                    strInPros += ",in_debit_o=0";
                    strInPros += ",in_invoice_o=0";
                    strInPros += ",in_invoice_tax_o=0";
                    strInPros += ",in_payment_o=0";
                    strInPros += ",in_retention_o=0";
                    strInPros += ",in_tax_o=0";
                    
                    break;
                case "In_Employee_Payment":
                    strRelName="In_Employee_Payment_item";
                    strRelPros = "in_money_tax=0";
                     strInPros = "in_act_collection_o=0";
                     strInPros += ",in_money_tax=0";
                    strMethod= "In_Vendor_Payment_Total";
                    break;
                default:
                    break;
            }
            
            
      
            if(this.getType()=="In_Employee_Payment")
            {
                this.apply("In_EmployeePayment_Compute");
            }
            else
            {
                
                this.apply("In_PaymentCompute");
                this.apply("In_UpdateContractMomey");
            }
            
            sql = "Update [" + strRelName + "] set " + strRelPros + " where source_id='" + this.getID() + "'\n";
            inn.applySQL(sql);

            sql += "Update [" +this.getType() + "] set " + strInPros + " where id='" + this.getID() + "'";
            inn.applySQL(sql);
            
            
			break;
			
		default:
			break;
		
	}
	
	if(err!="")
		throw new Exception(err);


		
}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
	throw new Exception(ex.InnerException==null?ex.Message:ex.InnerException.Message);
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return this;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Payment_PostPromote' and [Method].is_current='1'">
<config_id>5465E8EC60304E2A9985C54F5E8C06B9</config_id>
<name>In_Payment_PostPromote</name>
<comments>Payment_PostPromote</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
