/*
目的:更新使用者資訊
*/
//System.Diagnostics.Debugger.Break();

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

string aml ="<AML>";
aml += "<Item type='User' action='get'>";
aml += "</Item></AML>";
Item userChildren = inn.applyAML(aml);

for(int i=0;i<userChildren.getItemCount();i++){

    Item userChild = userChildren.getItemByIndex(i);

    string strUserID = userChild.getProperty("id","");
    string strUserName = userChild.getProperty("first_name","");

    Item itmSalesIdentity = _InnH.GetIdentityByUserId(strUserID);


    userChild.apply("edit");
    itmSalesIdentity.apply("edit");
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_UpdateUserInfo' and [Method].is_current='1'">
<config_id>0FE5BE356921486E988997FEAA0A4525</config_id>
<name>In_UpdateUserInfo</name>
<comments>inn</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
